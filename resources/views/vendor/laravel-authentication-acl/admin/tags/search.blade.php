<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title bariol-bold"><i class="fa fa-search"></i> Category search</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['route' => 'categories.list','method' => 'get']) !!}
        <!-- name text field -->
        <div class="form-group">
            {!! Form::label('category_name','Category name:') !!}
            {!! Form::text('category_name', null, ['class' => 'form-control', 'placeholder' => 'category name']) !!}
        </div>
        <span class="text-danger">{!! $errors->first('category_name') !!}</span>
        {!! Form::submit('Search', ["class" => "btn btn-info pull-right"]) !!}
        {!! Form::close() !!}
    </div>
</div>