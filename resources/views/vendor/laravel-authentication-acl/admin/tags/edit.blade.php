@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: edit tag
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        {{-- model general errors from the form --}}
        @if($errors->has('model') )
        <div class="alert alert-danger">{!! $errors->first('model') !!}</div>
        @endif

        {{-- successful message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
        <div class="alert alert-success">{{$message}}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                    <h3 class="panel-title bariol-bold">{!! isset($tag->id) ? '<i class="fa fa-pencil"></i> Edit' : '<i class="fa fa-users"></i> Create' !!} tool</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        {{-- tag base form --}}
                        
                        {!! Form::model($tag, [ 'url' => [URL::route('tags.edit'), $tag->id], 'method' => 'post'] ) !!}
                        <!-- name text field -->
                        <div class="form-group">
                            {!! Form::label('tag_name','Tool tag name: *') !!}
                            {!! Form::text('tag_name', null, ['class' => 'form-control', 'placeholder' => 'tool tag name']) !!}
                        </div>
                        <span class="text-danger">{!! $errors->first('tag_name') !!}</span>
                        {!! Form::hidden('id') !!}
                        <a href="{!! URL::route('tags.delete',['id' => $tag->id, '_token' => csrf_token()]) !!}" class="btn btn-danger pull-right margin-left-5 delete">Delete</a>
                        {!! Form::submit('Save', array("class"=>"btn btn-info pull-right ")) !!}
                        {!! Form::close() !!}
                    </div>                    
                </div>
           </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script>
    $(".delete").click(function(){
        return confirm("Are you sure to delete this item?");
    });
</script>
@stop
