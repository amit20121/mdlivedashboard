<div class="row margin-bottom-12">
    <div class="col-md-12">
        <a href="{!! URL::route('tags.new') !!}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New</a>
    </div>
</div>
@if(! $tags->isEmpty() )
<table class="table table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Operations</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tags as $tag)
        <tr>
            <td style="width:90%">{!! $tag->tag_name !!}</td>
            <td style="width:10%">            
                <a href="{!! URL::route('tags.edit', ['id' => $tag->id]) !!}"><i class="fa fa-edit fa-2x"></i></a>
                <a href="{!! URL::route('tags.delete',['id' => $tag->id, '_token' => csrf_token()]) !!}" class="margin-left-5 delete"><i class="fa fa-trash-o fa-2x"></i></a>
                <span class="clearfix"></span>            
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="paginator">
    {!! $tags->appends($request->except(['page']) )->render() !!}
</div>
@else
<span class="text-warning"><h5>No results found.</h5></span>
@endif
<div class="paginator">

</div>
