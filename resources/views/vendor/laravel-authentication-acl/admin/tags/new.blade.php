@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: add tag
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        {{-- model general errors from the form --}}
        @if($errors->has('model') )
        <div class="alert alert-danger">{!! $errors->first('model') !!}</div>
        @endif

        {{-- successful message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
        <div class="alert alert-success">{{$message}}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                    <h3 class="panel-title bariol-bold"><i class="fa fa-users"></i> Create tag</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        {{-- group base form --}}
                        
                        {!! Form::model([ 'url' => [URL::route('tags.new')], 'method' => 'post'] ) !!}
                        <!-- name text field -->
                        <div class="form-group">
                            {!! Form::label('tag_name','Name: *') !!}
                            {!! Form::text('tag_name', null, ['class' => 'form-control', 'placeholder' => 'tool tag name']) !!}
                        </div>
                        <span class="text-danger">{!! $errors->first('tag_name') !!}</span>
                        {!! Form::hidden('id') !!}
                        
                        {!! Form::submit('Save', array("class"=>"btn btn-info pull-right ")) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script>
    $(".delete").click(function(){
        return confirm("Are you sure to delete this item?");
    });
</script>
@stop
