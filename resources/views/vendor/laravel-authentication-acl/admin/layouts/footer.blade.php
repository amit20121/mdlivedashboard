<footer id="footer-bottom">
    <div class="container">
        <div class="row">
            <!--            <div class="col-md-6 col-sm-5">                
                            {{ HTML::image('images/footer-logo.jpg') }}
                        </div>-->
            <div class="col-md-6 col-sm-5">
                <p><strong>FOR INTERNAL USE ONLY</strong><br>COPYRIGHT &copy; {{ date("Y") }} MDLIVE INC. ALL RIGHTS RESERVED</p>
            </div>
            <div class="col-md-6 col-sm-7">
                <ul>
                    <li><a href="https://mdlive.com/consumer/privacy.html" target="_blank">Privacy Policy</a></li>
                    <li><a href="https://mdlive.com/consumer/terms.html" target="_blank">Terms of Use</a></li>
                    <li><a href="https://mdlive.com/consumer/disclaimers.html" target="_blank">Disclaimer</a></li>
                    <li><a href="https://mdlive.com/consumer/informed_consent.html" target="_blank">Informed Consent</a></li>
                </ul>
            </div>
        </div>      
    </div><!-- /container -->
</footer>     
<!--/Footer-->