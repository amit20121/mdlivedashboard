<?php
$current_route = \Request::route()->getName();
$authentication = \App::make('authenticator');
$user = $authentication->getLoggedUser();
$user_group = strtolower($user->groups()->first()->name);
if ($user_group == "admin" || $user_group == "superadmin" || $user_group == "health system admin" || $user_group == "health plan admin" || $user_group == "employer admin") {
    
} else {
    ?>
    <style>
        #addtlLinks a:first-child{display: none !important;}
    </style>
<?php } ?>

<div id="header">
    <div class="container">
        <a href="/" id="logo">
            {{ HTML::image('images/md-live-logo@2.png') }}
        </a>
        <div id="hdrRight" class="right">
            <div id="addtlLinks">
                @if(isset($menu_items))
                @foreach($menu_items as $item)
                <a class="{!! LaravelAcl\Library\Views\Helper::get_active_route_name($item->getRoute()) !!}" href="{!! $item->getLink() !!}">{!!$item->getName()!!}</a>
                @endforeach
                @endif   

                <a href="{!! URL::route('users.selfprofile.edit') !!}">My Account</a>
                <a href="{!! URL::route('contact.form') !!}">Contact Us</a>
                <a href="{!! URL::route('user.logout') !!}">Sign Out</a>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div style="clear:both;"></div>
    </div>
    <div class="gradTop">
        <div id="navWrap">
            <div class="container">
                <div id="mobileMenuBtn"><i class="fa fa-bars"></i></div>
                <div id="navigation-container">
                    <div id="navMiniMobile"> <a href="{!! URL::route('templates.list') !!}">My Template</a><span> | <a href="{!! URL::route('users.selfprofile.edit') !!}">My Account</a><span> | </span><a href="#">Contact Us</a><span> | </span><a href="{!! URL::route('user.logout') !!}">Sign Out</a></div>
                    <ul class="menu">
                        <li <?php if (isset($current_route) && $current_route == "home"): ?>class="active"<?php endif; ?>><a href="{!! URL::route('home') !!}">Home</a><li>       
                        <li <?php if (isset($current_route) && $current_route == "howtouse"): ?>class="active" <?php endif; ?>><a href="{!! URL::route('howtouse') !!}">How to use</a><li>       
                        <li <?php if (isset($current_route) && $current_route == "encourageactivation"): ?>class="active" <?php endif; ?>><a href="{!! URL::route('encourageactivation') !!}">Encourage Activation</a><li>       
                        <li <?php if (isset($current_route) && $current_route == "promoteutilization"): ?>class="active" <?php endif; ?>><a href="{!! URL::route('promoteutilization') !!}">Promote Utilization</a><li>       
                        <li <?php if (isset($current_route) && $current_route == "brand-asset"): ?>class="active" <?php endif; ?>><a href="{!! URL::route('brand-asset') !!}">Market the brand</a><li>       
                            <li <?php if (isset($current_route) && $current_route == "news-media"): ?>class="active" <?php endif; ?>><a href="{!! URL::route('news-media') !!}">News & Media</a><li>
                            @if($user_group == "admin" || $user_group=="superadmin" || $user_group == "health plan admin" || $user_group == "employer admin" || $user_group == "health system admin")  
                            @else
                        <li <?php if (isset($current_route) && $current_route == "dashboard.default"): ?>class="active" <?php endif; ?>><a href="{{URL::route('dashboard.default')}}">MARCOM Builder</a>
                             <ul style="display: none; width:180px;" class="sub-menu dropdown-menunew">
                                <li><a href="{{URL::route('dashboard.default')}}">Build your own</a></li>
                                <li><a href="{{URL::route('dashboard.default')}}">Let us do</a></li>
                                <li><a href="{{ URL::route('users.uploads') }}">Upload your own</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>       
                </div>
            </div>				
        </div>
    </div>
</div>