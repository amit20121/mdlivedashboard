<!--Header-->
<div id="header">
    <div class="container">
        <a href="/" id="logo">
            {{ HTML::image('images/md-live-logo@2.png') }}
        </a>
        <div id="hdrRight" class="right">
            <div id="addtlLinks">
                <a href="{!! URL::route('templates.list') !!}">My Template</a><span> | <a href="{!! URL::route('users.selfprofile.edit') !!}">My Account</a><span> | </span><a href="#">Contact Us</a><span> | </span><a href="{!! URL::route('user.logout') !!}">Sign Out</a>
            </div>
            <div style="clear:both;"></div>
        </div>
        <div style="clear:both;"></div>
    </div>
    <div class="gradTop">
        <div id="navWrap">
            <div class="container">
                <div id="mobileMenuBtn"><i class="fa fa-bars"></i></div>
                <div id="navigation-container">
                    <div id="navMiniMobile"> <a href="{!! URL::route('templates.list') !!}">My Template</a><span> | <a href="{!! URL::route('users.selfprofile.edit') !!}">My Account</a><span> | </span><a href="#">Contact Us</a><span> | </span><a href="{!! URL::route('user.logout') !!}">Sign Out</a></div>
                    <ul class="menu">                            
                        @if(isset($menu_items))
                        @foreach($menu_items as $item)
                        <li class="{!! LaravelAcl\Library\Views\Helper::get_active_route_name($item->getRoute()) !!}"> <a href="{!! $item->getLink() !!}">{!!$item->getName()!!}</a></li>
                        @endforeach
                        @endif       
                    </ul>       
                </div>
            </div>				
        </div>
    </div>
</div>
<!--/Header-->