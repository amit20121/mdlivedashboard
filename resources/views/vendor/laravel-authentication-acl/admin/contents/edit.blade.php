@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: edit content
@stop

@section('head_css')  
{!! HTML::style('vendor/backpack/colorbox/example2/colorbox.css') !!}
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        {{-- model general errors from the form --}}
        @if($errors->has('model') )
        <div class="alert alert-danger">{!! $errors->first('model') !!}</div>
        @endif

        {{-- successful message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
        <div class="alert alert-success">{{$message}}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-bold">{!! isset($content->id) ? '<i class="fa fa-pencil"></i> Edit' : '<i class="fa fa-users"></i> Create' !!} content</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        {{-- content base form --}}

                        {!! Form::model($content, [ 'url' => [URL::route('contents.edit'), $content->id], 'method' => 'post', 'files' => true] ) !!}

                        <div class="form-group">
                            {!! Form::label('tag_id','Select a tag: *') !!}
                            {!! Form::select('tag_id', $tag_values, $content->tag_id, ["class"=>"form-control permission-select"]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('campaign_id','Select a content type: *') !!}
                            {!! Form::select('campaign_id', $campaign_values, $content->campaign_id, ["class"=>"form-control permission-select"]) !!}
                        </div> 
                        <div class="form-group">
                            {!! Form::checkbox('default', 1, null, ['class' => '']) !!}
                            {!! Form::label('default','Select to make default') !!}
                        </div>
                        @if($user_group == "admin" || $user_group == "superadmin")
                        <div class="form-group">
                            {!! Form::label('verticles','Select a verticles: *') !!}
                            {!! Form::select('verticles', $verticle_values, $content->verticles, ["class"=>"form-control permission-select"]) !!}
                        </div>
                        @else
                        @if($user_group == "health system admin")
                        {!! Form::hidden('verticles', 'health_system') !!}
                        @elseif($user_group == "health plan admin")
                        {!! Form::hidden('verticles', 'health_plan') !!}
                        @else
                        {!! Form::hidden('verticles', str_replace(" ", "_", strtolower($user_group))) !!}
                        @endif
                        @endif
                        <!--                        <div class="form-group">
                                                    {!! Form::label('saveas','Select a content output format: *') !!}
                                                    {!! Form::select('saveas', $content_output_values, $content->saveas, ["class"=>"form-control permission-select"]) !!}
                                                </div>-->
                        <div class="form-group">
                            {!! Form::label('downloadable_content','Upload downloadable content: ') !!}
                            {!! Form::file('downloadable_content') !!}
                        </div>
                        @if(isset($content->downloadable_content) && !empty($content->downloadable_content))
                        <a href="{!! URL::route('contents.download', ['id' => $content->id]) !!}">Click to view downloadable content</a><br>
                        @endif


                        <div class="form-group">

                            {!! Form::label('thumbnail','Content Thumbnail *: ') !!}                                
                            {!! Form::text('thumbnail', null, ['id' => 'image-filemanager', 'class' => 'form-control', 'readonly' => 'readonly']) !!}
                            <div class="btn-group" role="group" aria-label="..." style="margin-top: 3px;">
                                <button type="button" data-inputid="image-filemanager" class="btn btn-default popup_selector">
                                    <i class="fa fa-cloud-upload"></i> Browse uploads</button>
                                <button type="button" data-inputid="image-filemanager" class="btn btn-default clear_elfinder_picker clearbtn">
                                    <i class="fa fa-eraser"></i> Clear</button>
                            </div>
                        </div>



                        @if(isset($content->thumbnail) && !empty($content->thumbnail))
                        <div class="thumbnailarea">
                            <img src="<?php echo URL::to('/') . "/" . $content->thumbnail; ?>" width="100" height="100">
                        </div>
                        @endif

                        <p class="text-danger">{!! $errors->first('thumbnail') !!}</p>                        


                        <div class="form-group">
                            {!! Form::label('content_title','Content title: *') !!}
                            {!! Form::text('content_title', null, ['class' => 'form-control', 'placeholder' => 'content title']) !!}
                        </div>
                        <span class="text-danger">{!! $errors->first('content_title') !!}</span>
                        <div class="form-group">
                            <div class="form-group">
                                {!! Form::label('description','Content: *') !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control tinymce', 'placeholder' => 'content descriptions']) !!}
                            </div>
                            <span class="text-danger">{!! $errors->first('description') !!}</span>
                            {!! Form::hidden('id') !!}
                            {!! Form::hidden('old_thumbnail', $content->thumbnail) !!}
                            {!! Form::hidden('old_downloadable_content', $content->downloadable_content) !!}
                            <a href="{!! URL::route('contents.delete',['id' => $content->id, '_token' => csrf_token()]) !!}" class="btn btn-danger pull-right margin-left-5 delete">Delete</a>
                            {!! Form::submit('Save', array("class"=>"btn btn-info pull-right ")) !!}
                            {!! Form::close() !!}
                        </div>                    
                    </div>  
                </div>
            </div>
        </div>   
    </div>
    @stop

    @section('footer_scripts')
    {!! HTML::script('vendor/backpack/tinymce/tinymce.min.js') !!}
        {!! HTML::script('vendor/backpack/colorbox/jquery.colorbox-min.js') !!}
    
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea.tinymce",
            skin: "dick-light",
            plugins: "image,link,media,anchor,code",
            file_browser_callback: elFinderBrowser,
        });

        function elFinderBrowser(field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: '{{ route('elfinder.tinymce4') }}', // use an absolute path!
                title: 'elFinder 2.0',
                width: 900,
                height: 450,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
            return false;
        }

        $(".delete").click(function () {
            return confirm("Are you sure to delete this content?");
        });
    </script>
        {!! HTML::script('packages/barryvdh/elfinder/js/standalonepopup.js') !!}
    @stop
