@extends('admin.layouts.base-1cols')

@section('title')
Admin area: dashboard
@stop

@section('content')
<div class="container">
    <div class="admin-area build">
        <div class="row">
            <div class="col-sm-12">
                <h2 id="pageTitle">MARCOM Builder</h2>
                <section class="paddedTopBottom-sm gt-s">
                    <div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="white-box {!! $page->content_top_bar !!}">
                                    {!! $page->content !!}      
                                </div>
                                {{--<div class="white-box green">
                                    <h3 class="green-txt">Select A Medium</h3>
                                    <p>Choose from a wide library of written pieces, including HTML blogs, e-mails, Web banners, flyers, posters, social media banners, and more!</p>
                                    <!--<a class="btn gt-started" href="{{URL::route('dashboard.default')}}">Build your own</a>-->
                            </div>--}}
                        </div>
                        <div class="col-sm-4">
                            <div class="white-box {!! $page->customize_top_bar !!}">
                                {!! $page->customize !!}    
                            </div>
                            {{-- <div class="white-box blue">
                                    <h3 class="blue-txt">Customize Your Selection</h3>
                                    <p>“Drag-and-drop” pre-written blocks of text into your template, and personalize your pieces with your logo, phone number, colors, and more—in real-time!</p>
                                    <!--<a class="btn gt-started" href="{{URL::route('dashboard.default')}}">Let us do</a>-->
                        </div>--}}
                    </div>
                    <div class="col-sm-4">
                        <div class="white-box {!! $page->sdd_top_bar !!}">
                            {!! $page->sdd !!}    
                        </div>
                        {{--<div class="white-box purple">
                                    <h3 class="purple-txt">Save, Download, And Distribute</h3>
                                    <p>Save your “finals” to your profile page for future use, or download your files to your PC to send to a commercial printer, print on a personal printer, broadcast via e-mail, or post on your own Intranet or website. </p>
                                    <!--<a class="btn gt-started" href="{{ URL::route('users.uploads') }}">Upload your own</a>-->
                    </div>--}}
            </div>
        </div>
    </div>
</section>
</div>
<div class="col-sm-12">
    <h2 id="pageTitle">Let's Get Started!</h2>
    <h5 class="subtitle">What Type Of Materials Would You Like To Build?</h5>
    <ul class="templates-choice" >
        @foreach($contents as $content)
        <li>
            <div class="hover">
                @if(isset($content->thumbnail) && !empty($content->thumbnail))
                <img src="/<?php echo $content->thumbnail; ?>" alt="logo"> 
                @else
                <img src="/images/no-img.png" alt="logo"> 
                @endif
                @if($content->downloadable_content != "")
                <div class="tmplates-link"><a href="{!! URL::route('contents.download', ['id' => $content->id]) !!}" target="_blank"><p>{!! $content->content_title !!}</p></a></div>
                @else
                <div class="tmplates-link"><a href="{!! URL::route('templates.new', ['id' => $content->id]) !!}" target="_blank"><p>{!! $content->content_title !!}</p></a></div>
                @endif
            </div>
        </li>
        @endforeach
    </ul>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
@stop