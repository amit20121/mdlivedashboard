@extends('admin.layouts.base-2cols')

@section('title')
    Admin area: Media list
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        {{-- model general errors from the form --}}
        @if($errors->has('model') )
            <div class="alert alert-danger">{!! $errors->first('model') !!}</div>
        @endif

        {{-- successful message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
            <div class="alert alert-success">{{$message}}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-bold"><i class="glyphicon glyphicon-th-list"></i> Media List</h3>
            </div>
            <div class="panel-body">
                @if(! $medias->isEmpty() )
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Media Name</th>  
                            <th>Tag Name</th>  
                            <th>User</th>  
                            <th>Created Date</th>
                            <th>Operations</th>
                        </tr>
                    </thead>
                    <tbody>        
                        @foreach($medias as $media)
                        <tr>
                            <td style="width:25%"><a href="{!! URL::route('media.viewMedia', ['id' => $media->id]) !!}">{!! $media->image_title !!}</a></td>            
                            <td style="width:25%">{{ $media->tag }}</td>            
                            <td style="width:25%">{!! $media->user()->first()->email !!}</a></td>            
                            <td style="width:20%">{{ $media->created_at }}</a></td> 
                            <td style="width:20%">
                                <a href="{!! URL::route('media.viewMedia', ['id' => $media->id]) !!}"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="{!! URL::route('media.deleteMedia', ['id' => $media->id]) !!}" class="margin-left-5 delete"><i class="glyphicon glyphicon-trash"></i></a>
                                <span class="clearfix"></span>            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="paginator">
                    {!! $medias->appends($request->except(['page']) )->render() !!}
                </div>
                @else
                <span class="text-warning"><h5>No results found.</h5></span>
                @endif
                <div class="paginator"></div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('footer_scripts')
<script>
    $(".delete").click(function () {
        return confirm("Are you sure to delete this item?");
    });
</script>
@stop