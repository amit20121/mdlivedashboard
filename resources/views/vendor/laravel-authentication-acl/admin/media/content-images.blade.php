@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: Add Content Images
@stop

@section('content')
@include('tinymce::tpl')  
<div class="row">
    <div class="col-md-12">
        {{-- model general errors from the form --}}
        @if($errors->has('model') )
            <div class="alert alert-danger">{!! $errors->first('model') !!}</div>
        @endif

        {{-- successful message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
            <div class="alert alert-success">{{$message}}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-bold">Upload Media Images</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        {{-- group base form --}}

                        {!! Form::open(
                        array(
                        'route' => 'contents.uploadImages', 
                        'class' => '', 
                        'files' => true)) !!}

                        <div class="form-group">
                            {!! Form::label('image_title','Title: ') !!}
                            {!! Form::text('image_title', null, ['class' => 'form-control', 'placeholder' => 'Image name']) !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('tag','Tag: ') !!}
                            {!! Form::text('tag', null, ['class' => 'form-control', 'placeholder' => 'Tag name']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('image','Images: ') !!}
                            {!! Form::file('image') !!}
                        </div>
                        <span class="text-danger">{!! $errors->first('image') !!}</span>

                        {!! Form::submit('Save', array("class"=>"btn btn-info pull-right ")) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script>
    $(".delete").click(function () {
        return confirm("Are you sure to delete this content?");
    });
</script>
@stop