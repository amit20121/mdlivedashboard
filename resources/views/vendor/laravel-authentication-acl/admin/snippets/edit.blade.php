@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: edit content
@stop

@section('head_css')  
{!! HTML::style('css/prism.css') !!}
{!! HTML::style('css/chosen.css') !!}
{!! HTML::style('vendor/backpack/colorbox/example2/colorbox.css') !!}
@stop

@section('content')
@include('tinymce::tpl')  
<div class="row">
    <div class="col-md-12">
        {{-- model general errors from the form --}}
        @if($errors->has('model') )
        <div class="alert alert-danger">{!! $errors->first('model') !!}</div>
        @endif

        {{-- successful message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
        <div class="alert alert-success">{{$message}}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-bold">{!! isset($content->id) ? '<i class="fa fa-pencil"></i> Edit' : '<i class="fa fa-users"></i> Create' !!} snippets</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        {{-- content base form --}}
                        <?php $content ?>

                        {!! Form::model($content, [ 'url' => [URL::route('snippets.edit'), $content->id], 'method' => 'post', 'files' => true] ) !!}

                        {{ Form::hidden('Ctag_id', $content->tag_id) }}
                        {{ Form::hidden('Cverticles', $content->verticles) }}

                        <div class="form-group">
                            {!! Form::label('title','Snippet Title: *') !!}
                            {!! Form::text('title', null, [ 'class' => 'form-control', 'placeholder' => 'Snippet Title']) !!}
                            <span class="text-danger">{!! $errors->first('title') !!}</span>
                        </div>                        
                        
                        <div class="form-group">
                            {!! Form::label('category_id','Select a tag: *') !!}
                            {!! Form::select('tag_id', $tag_values, $content->tag_id, ["class"=>"form-control permission-select"]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('campaign_id','Select campaign type: *') !!}
                            {!! Form::select('campaign_id[]', $campaign_values, $content->campaign_id, ["class"=>"form-control permission-select chosen-select", 'multiple'=>'multiple']) !!}
                        </div> 
                        <span class="text-danger">{!! $errors->first('campaign_id') !!}</span>

                        @if($user_group == "admin" || $user_group == "superadmin")
                        <div class="form-group">
                            {!! Form::label('verticles','Select a verticles: *') !!}
                            {!! Form::select('verticles', $verticle_values, $content->verticles, ["class"=>"form-control permission-select"]) !!}
                        </div>
                        @else
                        @if($user_group == "health system admin")
                        {!! Form::hidden('verticles', 'health_system') !!}
                        @elseif($user_group == "health plan admin")
                        {!! Form::hidden('verticles', 'health_plan') !!}
                        @else
                        {!! Form::hidden('verticles', str_replace(" ", "_", strtolower($user_group))) !!}
                        @endif
                        @endif
                        <div class="form-group">

                                {!! Form::label('thumbnail','Snippet Thumbnail *: ') !!}                                
                                {!! Form::text('thumbnail', null, ['id' => 'image-filemanager', 'class' => 'form-control', 'readonly' => 'readonly']) !!}
                                <div class="btn-group" role="group" aria-label="..." style="margin-top: 3px;">
                                    <button type="button" data-inputid="image-filemanager" class="btn btn-default popup_selector">
                                        <i class="fa fa-cloud-upload"></i> Browse uploads</button>
                                    <button type="button" data-inputid="image-filemanager" class="btn btn-default clear_elfinder_picker clearbtn">
                                        <i class="fa fa-eraser"></i> Clear</button>
                        </div>
                        </div>
                        
                        
                        
                        @if(isset($content->thumbnail) && !empty($content->thumbnail))
                        <div class="thumbnailarea">
                        <img src="<?php echo URL::to('/') . "/" . $content->thumbnail; ?>" width="100" height="100">
                        </div>
                        @endif

                        <p class="text-danger">{!! $errors->first('thumbnail') !!}</p>

                        <div class="form-group">
                            {!! Form::label('description','Snippet description: *') !!}
                            {!! Form::textarea('description', null, ['class' => 'form-control tinymce', 'placeholder' => 'content descriptions']) !!}
                        </div>
                        <span class="text-danger">{!! $errors->first('description') !!}</span>
                        {!! Form::hidden('id') !!}
                        {!! Form::hidden('old_thumbnail', $content->thumbnail) !!}
                        {!! Form::hidden('old_downloadable_content', $content->downloadable_content) !!}
                        <a href="{!! URL::route('snippets.delete',['id' => $content->id, '_token' => csrf_token()]) !!}" class="btn btn-danger pull-right margin-left-5 delete">Delete</a>
                        {!! Form::submit('Save', array("class"=>"btn btn-info pull-right ")) !!}
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop

    @section('footer_scripts')
    {!! HTML::script('js/chosen.jquery.js') !!}
    {!! HTML::script('js/prism.js') !!}
    {!! HTML::script('vendor/backpack/colorbox/jquery.colorbox-min.js') !!}

    <script>
        $(".delete").click(function () {
            return confirm("Are you sure to delete this snippet?");
        });

        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
        
        $(".clearbtn").click(function(){
            $(".thumbnailarea").html('');
        });
        
    </script>
    {!! HTML::script('packages/barryvdh/elfinder/js/standalonepopup.js') !!}
    @stop