@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: Snippet List
@stop

@section('head_css')  
<style>
    body.waitMe_body {overflow:hidden;height:100%}
    body.waitMe_body.hideMe {transition:opacity .2s ease-in-out;opacity:0}
    body.waitMe_body .waitMe_container:not([data-waitme_id]) {position:fixed;z-index:9989;top:0;bottom:0;left:0;right:0;background-color: rgba(255,255,255,0.9)}
    body.waitMe_body .waitMe_container:not([data-waitme_id]) > div {animation-fill-mode:both;position:absolute}
    body.waitMe_body .waitMe_container.progress > div {width:0;height:3px;top:0;left:0;background:#000;box-shadow:-5px 0 5px 2px rgba(0,0,0,.2);animation:progress_body 7s infinite ease-out}
    body.waitMe_body .waitMe_container.working > div {width:10%;height:3px;top:0;left:-10%;background:#000;box-shadow:-5px 0 5px 2px rgba(0,0,0,.2);animation:working_body 2s infinite linear}
    body.waitMe_body .waitMe_container.progress > div:after {content:'';position:absolute;top:0;bottom:60%;right:0;width:60px;border-radius:50%;opacity:.5;transform:rotate(3deg);box-shadow:#000 1px 0 6px 1px}
    body.waitMe_body .waitMe_container.img > div {width:100%;height:100%;text-align:center;background-position:center!important;background-repeat:no-repeat!important}
    body.waitMe_body .waitMe_container.text > div {width:100%;top:45%;text-align:center}
</style>

@stop


@section('content')

<div class="row">
    <div class="col-md-12">
        {{-- print messages --}}

        <div class="ReturnMessageHere"></div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title bariol-bold"><i class="fa fa-group"></i> Snippets Re-generator</h3>

            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        {{-- group base form --}}

                        <form id="generateSnippetForm">

                            <div class="form-group">
                                <?php array_unshift($SnippetFileInfo, "Select Snippet"); ?>
                                {!! Form::label('snippetFile','Select Snippet Template *') !!}
                                {!! Form::select('snippetFile', $SnippetFileInfo, '', ["class"=>"form-control permission-select"]) !!}
                            </div>
                            <div class="form-group">


                                @if (Session::has('message'))
                                <div class="alert alert-info">{{ Session::get('ReturnMessage') }}</div>
                                @endif

                            </div> 

                            <div class="form-group">

                                <div  class="col-md-6" id="loadingMessage" >

                                </div>

                                <div class="col-md-6">
                                    <input type="button" name="Generate" class="btn btn-info pull-right generatebtn" value="Generate">
                                    {{-- Form::submit('Generate', array("class"=>"btn btn-info pull-right generatebtn ")) --}}
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@stop


@section('footer_scripts')
<script>
    $(document).ready(function (e) {

        e.preventDefault;

        $(".generatebtn").click(function () {
            if (confirm("Are you sure to generate this snippet?")) {
                var effect = "text";
                $('body').addClass('waitMe_body');
                var img = '';
                var text = '';
                img = 'background-image:url(\'/images/anim-loading.gif\');height: 80px;background-repeat: no-repeat;background-position: center;';
                text = '';
                var elem = $('<div class="waitMe_container ' + effect + '"><div style="' + img + '">' + text + '</div></div>');
                $('body').prepend(elem);


                // $("#loadingMessage").html('<img  src="/images/anim-loading.gif" alt="Smiley face" height="40">');



                var token = '{{ Session::token() }}';
                var snippetFile = $("#snippetFile").val();

                if (snippetFile == 0) {
                    $(".ReturnMessageHere").addClass("alert alert-danger");
                    $(".ReturnMessageHere").html("You must have to select one snippet");
                    return;
                }

                var url = '{{route("snippets.generate")}}'
                $.ajax({
                    method: "POST",
                    url: url,
                    data: {
                        snippetFile: snippetFile,
                        _token: token},
                    success: function (data) {
                        $("#loadingMessage").html('');
                        $(".ReturnMessageHere").removeClass("alert alert-danger")
                        $(".ReturnMessageHere").addClass("alert alert-success");
                        $(".ReturnMessageHere").html(data);

                        $('body.waitMe_body').addClass('hideMe');
                        $('body.waitMe_body').find('.waitMe_container').remove();
                        $('body.waitMe_body').removeClass('waitMe_body hideMe');



                    }, error: function () {
                        $("#loadingMessage").html('');
                        $(".ReturnMessageHere").removeClass("alert alert-success")
                        $(".ReturnMessageHere").addClass("alert alert-danger");
                        $(".ReturnMessageHere").html("Fail to generate snippet template");

                        $('body.waitMe_body').addClass('hideMe');
                        $('body.waitMe_body').find('.waitMe_container:not([data-waitme_id])').remove();
                        $('body.waitMe_body').removeClass('waitMe_body hideMe');


                    }
                });
            }
        })


        $("#snippetFile").change(function () {
            $("#loadingMessage").html('');
            $(".ReturnMessageHere").removeClass("alert alert-success")
            $(".ReturnMessageHere").removeClass("alert alert-danger");
            $(".ReturnMessageHere").html('');
        })

    });

</script>
@stop