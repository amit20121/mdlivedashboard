<div class="row margin-bottom-12">
    <div class="col-md-6">
        <a href="{!! URL::route('snippets.generate') !!}" class="btn btn-info pull-left"><i class="fa fa-file-archive-o"></i>  Snippets Generate</a>
    </div>
    <div class="col-md-6">
        <a href="{!! URL::route('snippets.new') !!}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New</a>
    </div>
</div>
@if(! $contents->isEmpty() )
<table class="table table-hover">
    <thead>
        <tr>
            <th>Title</th>
            <th>Tag</th>
            <th>Campaign</th>
            <th>Verticle</th>
            <th>Operations</th>
        </tr>
    </thead>
    <tbody>
         
        @foreach($contents as $content)
        <tr>
            <td style="width:20%">{!! $content->title !!}</td>
            <td style="width:20%">{!! $content->tag->tag_name !!}</td>
            <td style="width:20%">{!! $content->campaign->campaign_name !!}</td>
            <td style="width:20%">{!! ucwords(str_replace("_", " ", $content->verticles)) !!}</td>
            <td style="width:10%">            
                <a href="{!! URL::route('snippets.edit', ['id' => $content->id]) !!}"><i class="fa fa-edit fa-2x"></i></a>
                <a href="{!! URL::route('snippets.delete',['id' => $content->id, '_token' => csrf_token()]) !!}" class="margin-left-5 delete"><i class="fa fa-trash-o fa-2x"></i></a>
                <span class="clearfix"></span>            
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="paginator">
    {!! $contents->appends($request->except(['page']) )->render() !!}
</div>
@else
<span class="text-warning"><h5>No results found.</h5></span>
@endif
<div class="paginator">

</div>