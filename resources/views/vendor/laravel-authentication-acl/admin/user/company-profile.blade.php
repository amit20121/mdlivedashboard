@extends('admin.layouts.base-1cols')

@section('title')
Admin area: Edit user profile
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        {{-- success message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
        <div class="alert alert-success">{!! $message !!}</div>
        @endif
        <div class="comMsg"></div>
        
        <div class="panel panel-info margin-top-10">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="panel-title bariol-bold"><i class="fa fa-user"></i> Company Profile</h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-9">                        
                        {!! Form::model($user_profile,['route'=>'users.company_profile.edit', 'method' => 'post', 'files' => true, 'id'=>'companyProfile']) !!}
                        <!-- password text field -->
                        <div class="form-group">
                            {!! Form::label('company_name','Company Name: ') !!}
                            <?php
                            if (isset($user_profile->company_name) && !empty($user_profile->company_name)) :
                                $data_company = json_decode($user_profile->company_name);
                                $company = json_decode($user_profile->company_name);
                                unset($company->Default);
                                $i = 1;
                                foreach ($company as $key => $cn) :
                                    ?>
                                    <p row="{{$i}}">
                                        {!! Form::text('company_name['.$i.']', $cn, ['id' =>'company_name' ,'class' => 'form-control company_name', 'placeholder' => '']) !!}
                                        <?php if(isset($data_company->Default) && $cn==$data_company->Default) : ?>    
                                            <input type="radio" name="company_name_default" checked="checked" value="<?php echo $data_company->Default; ?>">
                                        <?php else: ?>
                                            <input type="radio" name="company_name_default" >
                                        <?php endif;?>
                                        <a data-id='{{$user_profile->id}}' data-name="{{$cn}}" href="javascript:void(0)" class="glyphicon glyphicon-minus-sign remove" data-id="{{$user_profile->id}}"></a>
                                    </p>
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <p row="1">
                                    {!! Form::text('company_name[1]', null, ['id' =>'company_name' ,'class' => 'form-control company_name', 'placeholder' => '']) !!}
                                    {{ Form::radio('company_name_default','') }}
                                </p>
                            <?php endif; ?>
                            <a href="javascript:void(0)" class="addMore">Add More</a>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Your URL\'s','Your URL\'s: ') !!}
                            <?php
                            if (isset($user_profile->urls) && !empty($user_profile->urls)) :
                                $data_url = json_decode($user_profile->urls);
                                $urls = json_decode($user_profile->urls);
                                unset($urls->Default);
                                $i = 1;
                                foreach ($urls as $key => $url) :
                                    ?>
                                    <p row="{{$i}}">
                                        {!! Form::text('urls['.$i.']', $url, ['id' =>'urls' ,'class' => 'form-control urls', 'placeholder' => '']) !!}
                                        <?php if(isset($data_url->Default) && $url==$data_url->Default) : ?>    
                                            <input type="radio" name="urls_default" checked="checked" value="<?php echo $data_url->Default; ?>">
                                        <?php else: ?>
                                            <input type="radio" name="urls_default" value="">
                                        <?php endif;?>
                                        <a data-id='{{$user_profile->id}}' data-name="{{$url}}" href="javascript:void(0)" class="glyphicon glyphicon-minus-sign remove"></a>
                                    </p>
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <p row="1">
                                    {!! Form::text('urls[1]', null, ['id' =>'urls' ,'class' => 'form-control urls', 'placeholder' => '']) !!}
                                    {{ Form::radio('urls_default') }}
                                </p>
                            <?php endif; ?>
                            <a href="javascript:void(0)" class="addMore">Add More</a>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Logo(s)','Logo(s): ') !!}
                            <?php
                            if (isset($user_profile->logos) && !empty($user_profile->logos)) :
                                $data_logos = json_decode($user_profile->logos);
                                $logos = json_decode($user_profile->logos);
                                unset($logos->Default);
                                $i = 1;
                                foreach ($logos as $logo) :
                                    ?>
                                    {!! Form::hidden('old_logos['.$i.']', $logo,['class' => str_replace('.', '', $logo)]) !!}
                                    <p row="{{$i}}">
                                        <img type="file" data-name="{{$logo}}" width="70" height="70" src="{{Config::get('app.url')}}/logos/{{$logo}}">
                                        <?php if(isset($data_logos->Default) && $logo==$data_logos->Default) : ?>    
                                            <input type="radio" name="logos_default" checked="checked" value="<?php echo $data_logos->Default; ?>">
                                        <?php else: ?>
                                            <input type="radio" name="logos_default" value="">
                                        <?php endif;?>
                                        <a data-id='{{$user_profile->id}}' data-name="{{$logo}}" href="javascript:void(0)" class="glyphicon glyphicon-minus-sign remove"></a>
                                    </p>
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                    <p row="1" class="input-float-left">
                                    {!! Form::file('logos[1]', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    {{ Form::radio('logos_default') }}
                                </p>
                            <?php endif; ?>
                            <a href="javascript:void(0)" class="addMore">Add More</a>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Copay statement','Copay statement: ') !!}
                            <?php
                            if (isset($user_profile->copay_statement) && !empty($user_profile->copay_statement)) :
                                $data_copay_statement = json_decode($user_profile->copay_statement);
                                $copay_statements = json_decode($user_profile->copay_statement);
                                unset($copay_statements->Default);
                                $i = 1;
                                foreach ($copay_statements as $copay_statement) :
                                    ?>
                                    <p row="{{$i}}">
                                        {!! Form::text('copay_statement['.$i.']', $copay_statement, ['id' =>'copay_statement' ,'class' => 'form-control copay_statement', 'placeholder' => '']) !!}
                                        <?php if(isset($data_copay_statement->Default) && $copay_statement==$data_copay_statement->Default) : ?>    
                                            <input type="radio" name="copay_statement_default" checked="checked" value="<?php echo $data_copay_statement->Default; ?>">
                                        <?php else: ?>
                                            <input type="radio" name="copay_statement_default">
                                        <?php endif;?>
                                        <a data-id='{{$user_profile->id}}' data-name="{{$copay_statement}}" href="javascript:void(0)" class="glyphicon glyphicon-minus-sign remove"></a>
                                    </p>
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <p row="1">
                                    {!! Form::text('copay_statement[1]', null, ['id' =>'copay_statement' ,'class' => 'form-control copay_statement', 'placeholder' => '']) !!}
                                    {{ Form::radio('copay_statement_default') }}
                                </p>
                            <?php endif; ?>
                            <a href="javascript:void(0)" class="addMore">Add More</a>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Monikers','Monikers: ') !!}
                            <?php
                            if (isset($user_profile->monikers) && !empty($user_profile->monikers)) :
                                $data_monikers = json_decode($user_profile->monikers);
                                $monikers = json_decode($user_profile->monikers);
                                unset($monikers->Default);
                                $i = 1;
                                foreach ($monikers as $moniker) :
                                    ?>
                                    <p row="{{$i}}">
                                        {!! Form::text('monikers['.$i.']', $moniker, ['id' =>'monikers' ,'class' => 'form-control monikers', 'placeholder' => '']) !!}
                                        <?php if(isset($data_monikers->Default) && $moniker==$data_monikers->Default) : ?>    
                                            <input type="radio" name="monikers_default" checked="checked" value="<?php echo $data_monikers->Default; ?>">
                                        <?php else: ?>
                                            <input type="radio" name="monikers_default" value="">
                                        <?php endif;?>
                                        <a data-id='{{$user_profile->id}}' data-name="{{$moniker}}" href="javascript:void(0)" class="glyphicon glyphicon-minus-sign remove"></a>
                                    </p>
                                    <?php
                                    $i++;
                                endforeach;
                            else:
                                ?>
                                <p row="1">
                                    {!! Form::text('monikers[1]', null, ['id' =>'monikers' ,'class' => 'form-control monikers', 'placeholder' => '']) !!}
                                    {{ Form::radio('monikers_default') }}
                                </p>
                            <?php endif; ?>
                            <a href="javascript:void(0)" class="addMore">Add More</a>
                        </div>

                        {!! Form::hidden('user_id', $user_profile->user_id) !!}
                        {!! Form::hidden('id', $user_profile->id) !!}
                        {!! Form::button('Save',['class' =>'btn btn-info pull-right margin-bottom-30 company-profile']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
