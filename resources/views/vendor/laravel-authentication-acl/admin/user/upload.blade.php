<link rel="stylesheet" href="<?php echo asset('vendor/dropzoner/dropzone/dropzone.min.css'); ?>">
<script src="<?php echo asset('vendor/dropzoner/dropzone/dropzone.min.js'); ?>"></script>
<script src="<?php echo asset('vendor/dropzoner/dropzone/config.js'); ?>"></script>

@extends('admin.layouts.base-1cols')

@section('title')
Admin area: Edit user profile
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info margin-top-10">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="panel-title bariol-bold"><i class="fa fa-user"></i> Please drag and drop your printable documents (PDF, Docs, Powerpoint, Abode Illustrator, Microsoft Publisher) within the below box.</h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div>
                            <form action='<?php echo route("dropzoner.upload") ?>' class='dropzone' id="dropzonersDropzone">
                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                <div class="dz-message"></div>
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>
                                <div class="dropzone-previews" id="dropzonePreview"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

<!-- CSRF Token -->
<script>
    window.csrfToken = '<?php echo csrf_token(); ?>';
    window.dropzonerDeletePath = '<?php echo route('dropzoner.delete') ?>';
</script>




