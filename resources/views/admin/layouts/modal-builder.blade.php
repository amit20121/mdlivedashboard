<div class="modal fade" id="contentBuilderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="modal-title" id="exampleModalLabel">Media Library</h5>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('keyword', null, ['class' => 'form-control', 'placeholder' => 'Type something']) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary search">Search</button>
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="all-images"></div>   
            </div>
        </div>
    </div>
</div>