<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title bariol-bold"><i class="fa fa-search"></i> Tag search</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(['route' => 'tags.list','method' => 'get']) !!}
        <!-- name text field -->
        <div class="form-group">
            {!! Form::label('tag_name','Tag name:') !!}
            {!! Form::text('tag_name', null, ['class' => 'form-control', 'placeholder' => 'tag name']) !!}
        </div>
        <span class="text-danger">{!! $errors->first('tag_name') !!}</span>
        {!! Form::submit('Search', ["class" => "btn btn-info pull-right"]) !!}
        {!! Form::close() !!}
    </div>
</div>
