@extends('laravel-authentication-acl::admin.layouts.base-2cols')

@section('title')
Admin area: edit campaign
@stop

@section('content')

<div class="row">
    <div class="col-md-12">
        {{-- model general errors from the form --}}
        @if($errors->has('model') )
        <div class="alert alert-danger">{!! $errors->first('model') !!}</div>
        @endif

        {{-- successful message --}}
        <?php $message = Session::get('message'); ?>
        @if( isset($message) )
        <div class="alert alert-success">{{$message}}</div>
        @endif
        <div class="panel panel-info">
            <div class="panel-heading">
                    <h3 class="panel-title bariol-bold">{!! isset($campaign->id) ? '<i class="fa fa-pencil"></i> Edit' : '<i class="fa fa-users"></i> Create' !!} template</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-10 col-xs-12">                        
                        
                        {!! Form::model($campaign, [ 'url' => [URL::route('campaigns.edit'), $campaign->id], 'method' => 'post'] ) !!}
                        
                        <div class="form-group">
                            {!! Form::label('campaign_name','Campaign name: *') !!}
                            {!! Form::text('campaign_name', null, ['class' => 'form-control']) !!}
                            
                        </div>
                        <span class="text-danger">{!! $errors->first('campaign_name') !!}</span>
                        {!! Form::hidden('id') !!}
                        <a href="{!! URL::route('campaigns.delete',['id' => $campaign->id, '_token' => csrf_token()]) !!}" class="btn btn-danger pull-right margin-left-5 delete">Delete</a>
                        {!! Form::submit('Save', array("class"=>"btn btn-info pull-right ")) !!}
                        {!! Form::close() !!}
                    </div>                    
                </div>
           </div>
        </div>
    </div>
</div>
@stop

@section('footer_scripts')
<script>
    $(".delete").click(function(){
        return confirm("Are you sure to delete this campaign?");
    });
</script>
@stop