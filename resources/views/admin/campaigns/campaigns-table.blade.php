<div class="row margin-bottom-12">
    <div class="col-md-12">
        <a href="{!! URL::route('campaigns.new') !!}" class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add New</a>
    </div>
</div>
@if(! $campaigns->isEmpty() )
<table class="table table-hover">
    <thead>
        <tr>
            <th>Campaign name</th>            
            <th>Operations</th>
        </tr>
    </thead>
    <tbody>
        @foreach($campaigns as $campaign)
        <tr>
            <td style="width:90%">{!! $campaign->campaign_name !!}</td>            
            <td style="width:10%">            
                <a href="{!! URL::route('campaigns.edit', ['id' => $campaign->id]) !!}"><i class="fa fa-edit fa-2x"></i></a>
                <a href="{!! URL::route('campaigns.delete',['id' => $campaign->id, '_token' => csrf_token()]) !!}" class="margin-left-5 delete"><i class="fa fa-trash-o fa-2x"></i></a>
                <span class="clearfix"></span>            
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="paginator">
    {!! $campaigns->appends($request->except(['page']) )->render() !!}
</div>
@else
<span class="text-warning"><h5>No results found.</h5></span>
@endif
<div class="paginator">

</div>