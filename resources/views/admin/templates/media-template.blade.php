@if(!$images->isEmpty())
    <div class="paginator">
        {!! $images->appends($request->except(['page']) )->render() !!}
    </div>
    <ul>
        @foreach ($images as $image)
            <li>
                <img src="<?php echo URL::to('/') . "/content_builder_images/" . $image->image; ?>" height="130" alt="content_builder_images/{{$image->image}}">
                <p>{!!$image->image_title!!}</p>
            </li>
        @endforeach
    </ul>
    <div class="paginator">
        {!! $images->appends($request->except(['page']) )->render() !!}
    </div>
@else
    <span class="text-warning"><h5>No results found.</h5></span>
@endif