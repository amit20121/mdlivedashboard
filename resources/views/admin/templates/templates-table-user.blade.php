@if(! $templates->isEmpty() )
<table class="table table-hover">
    <thead>
        <tr>
            <th>Template name</th>                
            <th>Created Date</th>
            <th>Status</th>
            <th>Operations</th>
        </tr>
    </thead>
    <tbody>        
        @foreach($templates as $template)
        <tr>
            <td style="width:35%"><a href="{!! URL::route('templates.edit', ['id' => $template->id]) !!}" target="_blank">{!! $template->template_name !!}</a></td>
            <td style="width:25%">{{ $template->created_at }}</a></td>
            <td style="width:20%">{{ $template->status }}</a></td>
            <td style="width:20%">
                @if ( $template->status == "Approved")
                    @if ( $template->category_id == 4)
                    <a href="{!! URL::route('templates.download', ['id' => $template->id]) !!}"><i class="glyphicon glyphicon-download"></i></a>
                    @else
                    <a href="{!! URL::route('templates.print', ['id' => $template->id]) !!}"><i class="glyphicon glyphicon-print"></i></a>
                    @endif
                @endif
                <a href="{!! URL::route('templates.edit', ['id' => $template->id]) !!}" target="_blank"><i class="glyphicon glyphicon-pencil"></i></a>
                <a href="{!! URL::route('templates.delete',['id' => $template->id, '_token' => csrf_token()]) !!}" class="margin-left-5 delete"><i class="glyphicon glyphicon-trash"></i></a>
                <span class="clearfix"></span>            
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<div class="paginator">
    {!! $templates->appends($request->except(['page']) )->render() !!}
</div>
@else
<span class="text-warning"><h5>No templates found.</h5></span>
@endif
<div class="paginator"></div>
