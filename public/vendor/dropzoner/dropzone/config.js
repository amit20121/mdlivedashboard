Dropzone.options.dropzonersDropzone = {
    uploadMultiple: false, //will not work for multiple uploads
    parallelUploads: 100,
    maxFilesize: 8,
    acceptedFiles: 'image/*,application/*',
    previewsContainer: '#dropzonePreview',
    addRemoveLinks: true,
    dictRemoveFile: '',
    accept: function (file, done) {
        var thumbnail = jQuery('.dropzone .dz-preview.dz-file-preview .dz-image:last');
        switch (file.type) {
            case 'application/pdf':
                thumbnail.css('background', 'url(/images/pdf-icon.png');
                break;
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                thumbnail.css('background', 'url(/images/doc-icon.png');
                break;
        }

        done();
    },
    // The setting up of the dropzone
    init: function () {
        var myDropzone = this;
        this.on("removedfile", function (file) {
            var filename = file.name;
            jQuery.ajax({
                type: 'POST',
                url: window.dropzonerDeletePath,
                data: {name: filename, _token: window.csrfToken},
                dataType: 'html',
                success: function (data) {
                    var rep = JSON.parse(data);
                    if (rep.code == 200)
                    {
                    }
                }
            });
        });
        $.get('/admin/getphoto', function (data) {
//            console.log(data);
            var files = JSON.parse(data);
            for (var i = 0; i < files.length; i++) {

                var mockFile = {
                    url: files[i].url,
                    size: files[i].size,
                    name: files[i].name
                };
                var ext = files[i].name.split('.').pop();
                myDropzone.emit("addedfile", mockFile); //here I get the error
                if (ext == "pdf") {
                    myDropzone.emit("thumbnail", mockFile, "/images/pdf-icon.png");
                } else if (ext == "doc" || ext == "docx") {
                    myDropzone.emit("thumbnail", mockFile, "/images/doc-icon.png");
                } else {
                    myDropzone.emit("thumbnail", mockFile, "/user_uploads/" + files[i].name);
                }
//                myDropzone.emit("thumbnail", mockFile);
                myDropzone.emit("success", mockFile);
                var existingFileCount = 1; // The number of files already uploaded
            }

        });

    },
    error: function (file, response) {
        if (typeof (response) === "string")
            var message = response; //dropzone sends it's own error messages in string
        else
            var message = response.message;
        file.previewElement.classList.add("dz-error");
        _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];
            _results.push(node.textContent = message);
        }
        return _results;
    },
    success: function (file, response) {
        if (typeof (response) != "undefined") {
            file.serverId = response.filename;
            var aa = file.previewElement.querySelector("[data-dz-name]");
            var bb = file.previewElement.querySelector(".download-uploaded-file");
            aa.innerHTML = response.filename;
            bb.href = "/user_uploads/" +response.filename;
        }
    }
};




