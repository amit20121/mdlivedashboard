$('document').ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('form input').val()
        }
    });
    $(document).on('click', '.addMore', function (e) {
        var i = $(this).parent('div').find('p').length + 1;
        var j = $(this).parent('div').find('p').last().attr('row');
        j++;
        var field_type = $(this).parent('div').find('p input[type="text"], p img, p input[type="file"]').last().attr('type');
        var field_name = $(this).parent('div').find('p input[type="text"]').last().attr('id');
        if (field_type == "file") {
            $(this).parent('div').find('p').last().after('<p row="' + j + '" class="input-float-left"><input type="file" name="logos[' + j + ']"><input type="radio" name="logos_default"><a href="javascript:void(0)" class="glyphicon glyphicon-minus-sign remove"></a></p>');
        } else {
            var _default = $.trim(field_name + '_default');
            $(this).parent('div').find('p').last().after('<p row="' + j + '"><input type="' + field_type + '" id="' + field_name + '" name="' + field_name + '[' + j + ']" placeholder="" class="form-control ' + field_name + '"><input type="radio" name="' + _default + '"><a href="javascript:void(0)" class="glyphicon glyphicon-minus-sign remove"></a></p>');
        }
        return false;
    });

    $(document).on('click', '.remove', function (e) {
        var i = $(this).closest("div").find("p").length;
        var _id = $(this).attr('data-id');
        var _data_name = $(this).attr('data-name');
        var class_name = $(this).closest("p").find('input[type="text"]').attr('id');
        if (typeof (class_name) == "undefined") {
            class_name = "logos";
        }
        var self = this;
        if (i > 1) {
            if (typeof (_id) != "undefined" && typeof (_data_name) != "undefined") {
                if (confirm("Are you sure want to delete?")) {
                    $.ajax({
                        url: 'profile_delete',
                        type: "post",
                        dataType: 'json',
                        data: {id: _id, name: _data_name, column_name: class_name},
                        success: function (data) {
                            if (data == 1) {
                                var remv = _data_name.replace(".", "");
                                $('.' + remv).remove();
                                $(self).closest("p").remove();
                                return false;
                            }
                        }
                    });
                } else {
                    return false;
                }
            } else {
                $(this).parents('p').remove();
            }
        }
        return false
    });

    $(document).on('click', '#companyProfile input[type="radio"]', function (e) {
        var _value = $(this).prev().val();
        if (_value == "") {
            var _value = $(this).prev().attr('data-name');
        }
        if (_value != "") {
            _value = _value.replace("C:\\fakepath\\", "");
            $(this).closest("div").find('input[type="radio"]').removeAttr('value');
            $(this).val(_value);
        }
    });

    $(document).on('click', '.company-profile', function (e) {
        $('.alert-success').css("display", "none");
        validateFields();
    });

    function validateFields() {
        var error = 0;
        $.each($('.urls'), function () {
            var urls = $(this).val();
            if (urls != "" && !isValidURL(urls)) {
                $('.comMsg').html("<div class='alert alert-danger'>Please provide a valid url</div>");
                $('html, body').animate({scrollTop: $('.comMsg').position().top}, 'slow');
                error = 1;
            }
        });
        if (error == 1) {
            return false;
        }
        var company_length = $('#companyProfile').find('input[name="company_name_default"]').length;
        var company_name = $('input[name="company_name_default"]:checked').val();
        var urls_length = $('#companyProfile').find('input[name="urls_default"]').length;
        var urls = $('input[name="urls_default"]:checked').val();
        var logos = $('input[name="logos_default"]:checked').val();
        var logos_length = $('#companyProfile').find('input[name="logos_default"]').length;
        var copay_statement_length = $('#companyProfile').find('input[name="copay_statement_default"]').length;
        var copay_statement = $('input[name="copay_statement_default"]:checked').val();
        var monikers_length = $('#companyProfile').find('input[name="monikers_default"]').length;
        var monikers = $('input[name="monikers_default"]:checked').val();

        if (company_length > 1 && typeof (company_name) == "undefined") {
            $('.comMsg').html("<div class='alert alert-danger'>You have to select atleast one company name as default</div>");
            $('html, body').animate({scrollTop: $('.comMsg').position().top}, 'slow');
            return false
        } else if (urls_length > 1 && typeof (urls) == "undefined") {
            $('.comMsg').html("<div class='alert alert-danger'>You have to select atleast one url as default</div>");
            $('html, body').animate({scrollTop: $('.comMsg').position().top}, 'slow');
            return false
        } else if (copay_statement_length > 1 && typeof (copay_statement) == "undefined") {
            $('.comMsg').html("<div class='alert alert-danger'>You have to select atleast one copay statement as default</div>");
            $('html, body').animate({scrollTop: $('.comMsg').position().top}, 'slow');
            return false
        } else if (logos_length > 1 && typeof (logos) == "undefined") {
            $('.comMsg').html("<div class='alert alert-danger'>You have to select atleast one logo as default</div>");
            $('html, body').animate({scrollTop: $('.comMsg').position().top}, 'slow');
            return false
        } else if (monikers_length > 1 && typeof (monikers) == "undefined") {
            $('.comMsg').html("<div class='alert alert-danger'>You have to select atleast one moniker as default</div>");
            $('html, body').animate({scrollTop: $('.comMsg').position().top}, 'slow');
            return false
        } else {
            $("#companyProfile").submit();
        }
    }

    function isValidURL(url) {
        var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

        if (RegExp.test(url)) {
            return true;
        } else {
            return false;
        }
    }
});