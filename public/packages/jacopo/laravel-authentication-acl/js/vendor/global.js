$(window).on('hashchange', function () {
    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            getPosts(page);
        }
    }
});
function getPosts(page) {
    var keyword = $('input[name="keyword"]').val();
    if (keyword != "") {
        var query = "keyword=" + keyword + "&page=" + page + ""
    } else {
        var query = "page=" + page + ""
    }
    $.ajax({
        url: '/media/mediaPagination?' + query,
    }).done(function (data) {
        $('.all-images').html(data);
        location.hash = page;
    })
}
$(document).ready(function () {
    $(document).on('click', '.all-images .pagination a', function (e) {
        getPosts($(this).attr('href').split('page=')[1]);
        return false;
    });
    $(document).on('click', '.search', function (e) {
        var keyword = $('input[name="keyword"]').val();
        $.ajax({
            url: '/media/search',
            type: 'post',
            data: {keyword: keyword}
        }).done(function (data) {
            jQuery('.all-images').html(data);
        })
        return false;
    });
});