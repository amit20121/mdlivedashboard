<?php

namespace LaravelAcl\Authentication\Models;

namespace App;

//
//
//use LaravelAcl\Authentication\Models\User;
use LaravelAcl\Authentication\Models\Tag;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Model;

class Template extends Model {

    protected $fillable = ['template_content', 'tag_id', 'template_name', 'user_id', 'verticles'];

    /**
     * Get the Tag name .
     */
    public function tag() {
        return $this->belongsTo(Tag::class);
    }

    /**
     * Get the User name .
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

}
