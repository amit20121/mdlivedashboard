<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
//use App\User;
use App\Campaign;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use view,
    Redirect,
    Config;
use App\Repositories\CampaignRepository;

class CampaignController extends Controller {

    /**
     * The campaign repository instance.
     *
     * @var CampaignRepository
     */
    protected $campaigns;

    public function __construct(CampaignRepository $campaigns) {

        $this->campaigns = $campaigns;
        $this->sidebar = array(
            "Campaign List" => array('url' => route('campaigns.list'), 'icon' => '<i class="fa fa-users"></i>'),
            'Add New' => array('url' => route('campaigns.new'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );
    }

    public function getList(Request $request) {
        $campaigns = $this->campaigns->all($request->except(['page']));
        return View::make('admin.campaigns.index')->with(['campaigns' => $campaigns, "request" => $request]);
    }

    /**
     * Destroy the given campaign.
     *
     * @param  Request  $request
     * @param  Campaign  $campaign
     * @return Response
     */
    public function deleteCampaign(Request $request) {
        $this->campaigns->delete($request->get('id'));
        return Redirect::route('campaigns.list')->withMessage(Config::get('acl_messages.flash.success.campaign_delete_success'));
    }

    public function editCampaign(Request $request) {
        try {
            $obj = $this->campaigns->find($request->get('id'));
        } catch (UserNotFoundException $e) {
            $obj = new Campaign;
        }

        return View::make('admin.campaigns.edit')->with(["campaign" => $obj]);
    }

    public function newCampaign(Request $request) {
        return View::make('admin.campaigns.new');
    }

    public function postEditCampaign(Request $request) {
        $id = $request->get('id');

//        try {
//            $obj = $this->f->process($request->all());
//        } catch (JacopoExceptionsInterface $e) {
//            $errors = $this->f->getErrors();
//            // passing the id incase fails editing an already existing item
//            return Redirect::route("campaigns.edit", $id ? ["id" => $id] : [])->withInput()->withErrors($errors);
//        }
        Campaign::where('id', $id)                
                ->update([
                    'campaign_name' => $request->campaign_name  
                    ]);
        return Redirect::route('campaigns.edit', ["id" => $id])->withMessage(Config::get('acl_messages.flash.success.campaign_edit_success'));
    }

    public function postCampaign(Request $request) {
//        try {
//            $obj = $this->f->process($request->all());
//        } catch (JacopoExceptionsInterface $e) {
//            $errors = $this->f->getErrors();
//            // passing the id incase fails editing an already existing item
//            return Redirect::route("campaigns.new", [])->withInput()->withErrors($errors);
//        }

        $this->validate($request, [
            'campaign_name' => 'required',
        ]);
        $authentication = \App::make('authenticator');
        $u = $authentication->getLoggedUser();
        //echo $u->groups[0]->name;
        //dd( $u->email );
        $campaign = new Campaign;
        $this->campaigns->create([
            'campaign_name' => $request->campaign_name            
        ]);

        return Redirect::route('campaigns.list')->withMessage(Config::get('acl_messages.flash.success.campaign_new_success'));
    }

}
