<?php

namespace App\Http\Controllers;

use Illuminate\Support\MessageBag;
//use App\User;
use App\Template;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use view,
    Redirect,
    Config,
    Mail;
use LaravelAcl\Authentication\Models\Media;
use App\Repositories\TemplateRepository;
use LaravelAcl\Authentication\Repository\ContentRepository;
use LaravelAcl\Authentication\Repository\MediaRepository;
use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;

class TemplateController extends Controller {

    /**
     * The template repository instance.
     *
     * @var TemplateRepository
     */
    protected $templates;

    /**
     * The content repository instance.
     *
     * @var ContentRepository
     */
    protected $contents;
    protected $auth;
    protected $media;

    public function __construct(AuthenticateInterface $auth, TemplateRepository $templates, ContentRepository $contents, MediaRepository $media) {
        $this->auth = $auth;
        $this->templates = $templates;
        $this->contents = $contents;
        $this->media = $media;
        $this->sidebar = array(
            "Template List" => array('url' => route('templates.list'), 'icon' => '<i class="glyphicon glyphicon-th-list"></i>'),
                //'Add New' => array('url' => route('templates.new'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );
    }

    public function getList(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        if ($user_group == "admin" || $user_group == "superadmin") {
            $templates = $this->templates->all(null, $request->except(['page']));
            return View::make('admin.templates.index')->with(['templates' => $templates, "request" => $request, 'sidebar_items' => $this->sidebar]);
        } elseif ($user_group == "health system admin") {
            $templates = $this->templates->all(null, $request->except(['page']), "health_system");
            return View::make('admin.templates.index')->with(['templates' => $templates, "request" => $request, 'sidebar_items' => $this->sidebar]);
        } elseif ($user_group == "health plan admin") {
            $templates = $this->templates->all(null, $request->except(['page']), "health_plan");
            return View::make('admin.templates.index')->with(['templates' => $templates, "request" => $request, 'sidebar_items' => $this->sidebar]);
        } elseif ($user_group == "employer admin") {
            $templates = $this->templates->all(null, $request->except(['page']), "employer");
            return View::make('admin.templates.index')->with(['templates' => $templates, "request" => $request, 'sidebar_items' => $this->sidebar]);
        } else {
            $templates = $this->templates->all($user->id, $request->except(['page']));
            return View::make('admin.templates.index-user')->with(['templates' => $templates, "request" => $request, 'sidebar_items' => $this->sidebar]);
        }
    }

    /**
     * Destroy the given template.
     *
     * @param  Request  $request
     * @param  Template  $template
     * @return Response
     */
    public function deleteTemplate(Request $request) {
        $this->templates->delete($request->get('id'));
        return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.template_delete_success'));
    }

    public function editTemplate(Request $request) {
        $obj = $this->templates->find($request->get('id'));
        $logged_user = $this->auth->getLoggedUser();
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        if (isset($obj) && $obj->user_id == $logged_user->id) {
            if ($obj->tag_id == 11)
                return View::make('admin.templates.edit')->with(["template" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group, "els" => $user_group ."_social_posts"]);
            else if ($obj->tag_id == 4)
                return View::make('admin.templates.edit')->with(["template" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group, "els" => $user_group ."_html_emails"]);
            else
                return View::make('admin.templates.edit')->with(["template" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group, "els" => $user_group . "_snippets"]);
            
        }else {
            return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.template_edit_not_authorize'));
        }
    }

    public function newTemplate(Request $request) {
        $obj = $this->contents->find($request->get('id'));
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        if ($user_group == "health system") {
            $user_group = str_replace(" ", "_", $user_group);
        } elseif ($user_group == "health plan") {
            $user_group = str_replace(" ", "_", $user_group);
        }
        if ($obj->tag_id == 11)
            return View::make('admin.templates.new')->with(["content" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group, "els" => $user_group . "_social_posts"]);
        else if ($obj->tag_id == 4)
            return View::make('admin.templates.new')->with(["content" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group, "els" => $user_group . "_html_emails"]);
        else
            return View::make('admin.templates.new')->with(["content" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group, "els" => $user_group . "_snippets"]);
    }

    public function postEditTemplate(Request $request) {
        $id = $request->get('id');
        $this->validate($request, [
            'template_content' => 'required',
            'template_name' => 'required'
        ]);
        Template::where('id', $id)
                ->update([
                    'template_content' => $request->template_content,
                    'tag_id' => $request->tag_id,
                    'template_name' => $request->template_name,
                    'verticles' => $request->verticles
        ]);

        return Redirect::route('templates.edit', ["id" => $id])->withMessage(Config::get('acl_messages.flash.success.template_edit_success'));
    }

    public function postTemplate(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $this->validate($request, [
            'template_content' => 'required',
            'template_name' => 'required'
        ]);
        $template = new Template;

        $submit_return = $this->templates->create([
            'template_content' => $request->template_content,
            'tag_id' => $request->tag_id,
            'template_name' => $request->template_name,
            'verticles' => $request->verticles,
            'user_id' => $user->id,
        ]);

        if ($submit_return == true) {
            $user['template_name'] = $request->template_name;
            Mail::send('admin.emails.template-created', ['template_name' => $request->template_name, 'user' => $user], function ($m) use ($user) {
                $m->from($user->email, $name = null);
                $m->to(config('mail.from.address'))->cc('sam@bglobalsourcing.com');
                //$m->to('sahbajuddin@gmail.com');
                $m->subject($user->template_name . ' has been created for your approval');
            });
        }
        return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.template_new_success'));
    }

    public function printTemplate(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        try {
            $obj = $this->templates->find($request->get('id'));
        } catch (UserNotFoundException $e) {
            $obj = new Template;
        }

        if (isset($user_group) && ($user_group == "admin" || $user_group == "superadmin" || $user_group == "health system admin" || $user_group == "health plan admin" || $user_group == "employer admin")) {
            $url = route('templates.fetch', ['id' => $request->get('id')]);
            if ($obj->tag_id == 11) {
                //$file_name = str_replace(" ", "_", strtolower($obj->template_name)) . ".jpg";
                $pdf2 = \App::make('snappy.image.wrapper');
                $pdf2->loadFile($url);
                return $pdf2->download();
            } else {
                $file_name = str_replace(" ", "_", strtolower($obj->template_name)) . ".pdf";
                $pdf = \App::make('snappy.pdf.wrapper');
                //$pdf->setPaper('a3');
                $pdf->loadFile($url);
                return $pdf->download($file_name);
            }
        } else if (isset($obj) && !empty($obj) && $obj->status == "Approved") {
            $url = route('templates.fetch', ['id' => $request->get('id')]);
            if ($obj->tag_id == 11) {
                //$file_name = str_replace(" ", "_", strtolower($obj->template_name)) . ".pdf";
                $pdf2 = \App::make('snappy.image.wrapper');
                $pdf2->loadFile($url);
                return $pdf2->download();
            } else {
                $file_name = str_replace(" ", "_", strtolower($obj->template_name)) . ".pdf";
                $pdf = \App::make('snappy.pdf.wrapper');
                //$pdf->setPaper('a3');
                $pdf->loadFile($url);
                return $pdf->download($file_name);
            }
        } else {
            return Redirect::route('templates.list');
        }
    }

    public function fetchTemplate(Request $request) {
        try {
            $obj = $this->templates->find($request->get('id'));
        } catch (UserNotFoundException $e) {
            $obj = new Template;
        }

        return View::make('admin.templates.print')->with(["template" => $obj]);
    }

    public function approveForPrint(Request $request) {
        $obj = $this->templates->find($request->get('id'));
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        $status = $request->get('status');
        if ($status == "Approved") {
            $status = "Pending";
        } else {
            $status = "Approved";
        }
        if ($user_group == "admin" || $user_group == "superadmin" || $user_group == "health system admin" || $user_group == "health plan admin" || $user_group == "employer admin") {
            if (isset($obj) && !empty($obj)) {
                Template::where('id', $request->get('id'))
                        ->update([
                            'status' => $status
                ]);

                Mail::send('admin.emails.template-status', ['user' => $user, 'status' => $status], function ($m) use ($user, $status) {
                    $m->from($user->email, $name = null);
                    $m->to(config('mail.from.address'))->cc('sam@bglobalsourcing.com');
                    $m->subject('Template Approval');
                });
                return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.template_approval'));
            } else {
                return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.template_approval_error'));
            }
        }
    }

    public function getAllMedia(Request $request) {
        $_data = $request->except('_token');
        if ($_data['check'] == "yes") {
            $directory = public_path('content_builder_images');
            $images = $this->media->all($request->except(['page']));
            return View('admin.templates.media-template')->with(["images" => $images, "request" => $request]);
        }
    }

    public function mediaPagination(Request $request) {
        $directory = public_path('content_builder_images');
        $keyword = explode(" ", $request->keyword);
        if (isset($keyword[0]) && empty($keyword[0])) {
            $images = $this->media->all($request->except(['page']));
        } else {
            if (count($keyword) == 1) {
                $images = Media::where("image_title", "LIKE", "%{$request->get('keyword')}%")
                        ->orWhere("tag", "LIKE", "%{$request->get('keyword')}%")
                        ->orderBy('id', 'DESC')
                        ->paginate(Config::get('acl_base.contents_per_page'));
            } else {
                $images = Media::where("image_title", "LIKE", "%{$request->get('keyword')}%")
                        ->orWhere("tag", "LIKE", "%{$request->get('keyword')}%")
                        ->orWhereIn('tag', $keyword)
                        ->orderBy('id', 'DESC')
                        ->paginate(Config::get('acl_base.contents_per_page'));
            }
        }
        return View('admin.templates.media-template')->with(["images" => $images, "request" => $request]);
    }

    public function search(Request $request) {
        $input = $request->except('_token');
        $keyword = explode(" ", $input['keyword']);
        if (isset($keyword[0]) && empty($keyword[0])) {
            $images = $this->media->all($request->except(['page']));
        } elseif (isset($keyword[0]) && !empty($keyword[0]) && count($keyword) == 1) {
            $images = Media::where("image_title", "LIKE", "%{$request->get('keyword')}%")
                    ->orWhere("tag", "LIKE", "%{$request->get('keyword')}%")
                    ->orderBy('id', 'DESC')
                    ->paginate(Config::get('acl_base.contents_per_page'));
        } else {
            $images = Media::where("image_title", "LIKE", "%{$request->get('keyword')}%")
                    ->orWhere("tag", "LIKE", "%{$request->get('keyword')}%")
                    ->orWhereIn('tag', $keyword)
                    ->orderBy('id', 'DESC')
                    ->paginate(Config::get('acl_base.contents_per_page'));
        }
        return View('admin.templates.media-template')->with(["images" => $images, "request" => $request]);
    }

    public function downloadTemplate(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        try {
            $obj = $this->templates->find($request->get('id'));
        } catch (UserNotFoundException $e) {
            $obj = new Template;
        }
        $file_name = str_replace(" ", "_", $obj->template_name) . ".html";
        $content = $obj->template_content;
        $data['content'] = $content;
        if (isset($user_group) && ($user_group == "admin" || $user_group == "superadmin" || $user_group == "health system admin" || $user_group == "health plan admin" || $user_group == "employer admin")) {
            return Response()
                            ->view('admin.templates.download', $data)
                            ->header('Content-disposition', 'attachment; filename=' . $file_name)
                            ->header('Content-Type', 'text/html');
        } else if (isset($obj) && !empty($obj) && $obj->status == "Approved") {
            return Response()
                            ->view('admin.templates.download', $data)
                            ->header('Content-disposition', 'attachment; filename=' . $file_name)
                            ->header('Content-Type', 'text/html');
        } else {
            return Redirect::route('templates.list');
        }
    }

}
