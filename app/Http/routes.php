<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::get('/', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'home',
    'uses' => 'WelcomeController@getWecomePage'
]);
Route::get('/home', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'home',
    'uses' => 'WelcomeController@getWecomePage'
]);
Route::get('/admin/templates/list', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.list',
    'uses' => 'TemplateController@getList'
]);
Route::get('/admin/templates', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.list',
    'uses' => 'TemplateController@getList'
]);

Route::get('/admin/templates/edit', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.edit',
    'uses' => 'TemplateController@editTemplate'
]);
Route::post('/admin/templates/edit', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.edit',
    'uses' => 'TemplateController@postEditTemplate'
]);
Route::get('/admin/templates/delete', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.delete',
    'uses' => 'TemplateController@deleteTemplate'
]);
Route::get('/admin/templates/new', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.new',
    'uses' => 'TemplateController@newTemplate'
]);
Route::post('/admin/templates/new', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.new',
    'uses' => 'TemplateController@postTemplate'
]);
Route::get('/admin/templates/print', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.print',
    'uses' => 'TemplateController@printTemplate'
]);
Route::get('/admin/templates/download', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'templates.download',
    'uses' => 'TemplateController@downloadTemplate'
]);
Route::get('/admin/templates/fetch', [
    'as' => 'templates.fetch',
    'uses' => 'TemplateController@fetchTemplate'
]);
Route::get('/admin/templates/approve', [
    'as' => 'templates.approve',
    'uses' => 'TemplateController@approveForPrint'
]);

/*
 *
 * Campaign
 *  */
Route::get('/admin/campaigns/list', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'campaigns.list',
    'uses' => 'CampaignController@getList'
]);

Route::get('/admin/campaigns/edit', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'campaigns.edit',
    'uses' => 'CampaignController@editCampaign'
]);
Route::post('/admin/campaigns/edit', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'campaigns.edit',
    'uses' => 'CampaignController@postEditCampaign'
]);
Route::get('/admin/campaigns/delete', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'campaigns.delete',
    'uses' => 'CampaignController@deleteCampaign'
]);
Route::get('/admin/campaigns/new', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'campaigns.new',
    'uses' => 'CampaignController@newCampaign'
]);
Route::post('/admin/campaigns/new', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'campaigns.new',
    'uses' => 'CampaignController@postCampaign'
]);
Route::get('/how-to-use-hub', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'howtouse',
    'uses' => 'WelcomeController@howtouse'
]);
Route::get('/encourage-activation', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'encourageactivation',
    'uses' => 'WelcomeController@encourageactivation'
]);
Route::get('/promote-utilization', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'promoteutilization',
    'uses' => 'WelcomeController@promoteutilization'
]);
Route::get('/brand-assets', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'brand-asset',
    'uses' => 'WelcomeController@brandassets'
]);
Route::get('/news-media', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'news-media',
    'uses' => 'WelcomeController@newsmedia'
]);
Route::get('/admin/contact-us', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'contact.form', 
    'uses' => 'ContactController@create']);
Route::post('/admin/contact-us', [
    'middleware' => array('logged', 'can_see'),
    'as' => 'contact.store',
    'uses' => 'ContactController@store']);

Route::post('/media/getAllMedia', [
    'as' => 'media.getAllMedia',
    'uses' => 'TemplateController@getAllMedia'
]);

Route::get('/media/mediaPagination', [
    'as' => 'media.mediaPagination',
    'uses' => 'TemplateController@mediaPagination'
]);

Route::post('/media/search', [
    'middleware' => array('logged'),
    'as' => 'media.search',
    'uses' => 'TemplateController@search'
]);
