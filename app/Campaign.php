<?php
namespace LaravelAcl\Authentication\Models;
namespace App;

use LaravelAcl\Authentication\Models\Content;
use Illuminate\Database\Eloquent\Model;


class Campaign extends Model {

    protected $fillable = ['campaign_name'];
    

    /**
     * Get the Category name .
     */
    public function content() {
        return $this->hasMany(Content::class);
    }
}
