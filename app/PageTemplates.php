<?php

namespace App;

trait PageTemplates {
    /*
      |--------------------------------------------------------------------------
      | Page Templates for Backpack\PageManager
      |--------------------------------------------------------------------------
      |
      | Each page template has its own method, that define what fields should show up using the Backpack\CRUD API.
      | Use snake_case for naming and PageManager will make sure it looks pretty in the create/update form
      | template dropdown.
      |
      | Any fields defined here will show up after the standard page fields:
      | - select template
      | - page name (only seen by admins)
      | - page title
      | - page slug
     */

    private function home() {                
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>Content</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'content',
            'label' => 'Content',
            'type' => 'tinymce',
            'placeholder' => 'Your content here',
        ]);
        $this->crud->addField([
            'name' => 'short_description',
            'label' => 'Short description',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras',
        ]);
    }

    private function how_to_use() {
        $this->crud->addField([
            'name' => 'banner_title',
            'label' => 'Banner title',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_description',
            'label' => 'Banner description',
            'type' => 'textarea',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_image',
            'label' => 'Banner image',
            'type' => 'browse',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([   // CustomHTML
            'name' => 'content_separator',
            'type' => 'custom_html',
            'value' => '<br><h2>Easy as 1-2-3</h2><hr>',
        ]);
        $this->crud->addField([
            'name' => 'download',
            'label' => 'Step 1. Download',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras',
            'placeholder' => 'Your content here',
        ]);
        $this->crud->addField([
            'name' => 'customize',
            'label' => 'Step 2. Customize',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'customize_content1_image',
            'label' => 'Customize sample 1 preview image(before)',
            'type' => 'browse',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'customize_content1_image_after',
            'label' => 'Customize sample 1 preview image(after)',
            'type' => 'browse',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'customize_content2_image',
            'label' => 'Customize sample 2 preview image',
            'type' => 'browse',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'distribute_title',
            'label' => 'Step 3. Distribute',
            'type' => 'text',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'distribute',
            'label' => '',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras',
            'placeholder' => 'Your content here',
        ]);
    }
    
    private function encourage_activation() {
        $this->crud->addField([
            'name' => 'banner_title',
            'label' => 'Banner title',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_description',
            'label' => 'Banner description',
            'type' => 'textarea',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_image',
            'label' => 'Banner image',
            'type' => 'browse',
            'fake' => true,
            'store_in' => 'extras',
        ]);        
        $this->crud->addField([
            'name' => 'content',
            'label' => 'Consumer Overview',
            'type' => 'tinymce',            
        ]);
        $this->crud->addField([
            'name' => 'FAQ',
            'label' => 'FAQs',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
    }
    
    private function promote_utilization() {
        $this->crud->addField([
            'name' => 'banner_title',
            'label' => 'Banner title',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_description',
            'label' => 'Banner description',
            'type' => 'textarea',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_image',
            'label' => 'Banner image',
            'type' => 'browse',
            'fake' => true,
            'store_in' => 'extras',
        ]);        
        $this->crud->addField([
            'name' => 'content',
            'label' => 'Content',
            'type' => 'tinymce',            
        ]);
        $this->crud->addField([
            'name' => 'marcom_builder',
            'label' => 'MARCOM builder content',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
//        $this->crud->addField([   // CustomHTML
//            'name' => 'content_separator',
//            'type' => 'custom_html',
//            'value' => '<br><h2>MDLIVE Engagement Material</h2><hr>',
//        ]);
//        $this->crud->addField([
//            'name' => 'flyers_content',
//            'label' => 'Flyers content',
//            'type' => 'tinymce',
//            'fake' => true,
//            'store_in' => 'extras'
//        ]);
//        $this->crud->addField([
//            'name' => 'posters_content',
//            'label' => 'Posters content',
//            'type' => 'tinymce',
//            'fake' => true,
//            'store_in' => 'extras'
//        ]);
//        $this->crud->addField([
//            'name' => 'web_banners_content',
//            'label' => 'Web Banners content',
//            'type' => 'tinymce',
//            'fake' => true,
//            'store_in' => 'extras'
//        ]);
        $this->crud->addField([
            'name' => 'ready_to_go_campaigns',
            'label' => 'Ready to Go Campaigns content',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'admin_content',
            'label' => 'Campaigns content(Admin)',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'health_system_content',
            'label' => 'Health system campaigns content',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'health_plan_content',
            'label' => 'Health plan campaigns content',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'employer_content',
            'label' => 'Employer campaigns content',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'video_presentations',
            'label' => 'Video Presentations',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);
    }
    
    private function market_the_brand() {
        $this->crud->addField([
            'name' => 'banner_title',
            'label' => 'Banner title',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_description',
            'label' => 'Banner description',
            'type' => 'textarea',
            'fake' => true,
            'store_in' => 'extras',
        ]);
        $this->crud->addField([
            'name' => 'banner_image',
            'label' => 'Banner image',
            'type' => 'browse',
            'fake' => true,
            'store_in' => 'extras',
        ]);        
        $this->crud->addField([
            'name' => 'content',
            'label' => 'Logos',
            'type' => 'tinymce',            
        ]);
        $this->crud->addField([
            'name' => 'promotional_items',
            'label' => 'Promotional Items',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);        
    }
    private function marcom_builder() {              
        $this->crud->addField([
            'name' => 'content_top_bar',
            'label' => 'Topbar color',
            'type' => 'select_from_array',
            'options' => ['green' => 'Green', 'blue' => 'Blue', 'purple' => 'Purple'],
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'content',
            'label' => 'Select a medium',
            'type' => 'tinymce',            
        ]);
        $this->crud->addField([
            'name' => 'customize_top_bar',
            'label' => 'Topbar color',
            'type' => 'select_from_array',
            'options' => ['green' => 'Green', 'blue' => 'Blue', 'purple' => 'Purple'],
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'customize',
            'label' => 'Customize Your Selection',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);   
        $this->crud->addField([
            'name' => 'sdd_top_bar',
            'label' => 'Topbar color',
            'type' => 'select_from_array',
            'options' => ['green' => 'Green', 'blue' => 'Blue', 'purple' => 'Purple'],
            'fake' => true,
            'store_in' => 'extras'
        ]);
        $this->crud->addField([
            'name' => 'sdd',
            'label' => 'Save, Download, And Distribute',
            'type' => 'tinymce',
            'fake' => true,
            'store_in' => 'extras'
        ]);        
    }

}
