<?php namespace LaravelAcl\Authentication\Validators;

use Event;
use LaravelAcl\Library\Validators\AbstractValidator;

class ContentValidator  extends AbstractValidator
{
    protected static $rules = array(
        "description" => ["required"],
        "content_title" => ["required"],
    );

    public function __construct()
    {
        Event::listen('validating', function($input)
        {
            static::$rules["content_title"][] = "required";
            static::$rules["description"][] = "required";
        });
    }
} 