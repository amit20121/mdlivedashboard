<?php namespace LaravelAcl\Authentication\Validators;

use Event;
use LaravelAcl\Library\Validators\AbstractValidator;

class SnippetValidator  extends AbstractValidator
{
    protected static $rules = array(
        "description" => ["required"],
        "campaign_id" => ["required"],
        
    );

    public function __construct()
    {
        Event::listen('validating', function($input)
        {
            static::$rules["description"][] = "required";
            static::$rules["campaign_id"][] = "required";
           
        });
    }
} 