<?php namespace LaravelAcl\Authentication\Validators;

use Event;
use LaravelAcl\Library\Validators\AbstractValidator;

class TagValidator  extends AbstractValidator
{
    protected static $rules = array(
        "tag_name" => ["required"],
    );

    public function __construct()
    {
        Event::listen('validating', function($input)
        {
            static::$rules["tag_name"][] = "required";
        });
    }
} 
