<?php

namespace App;

namespace LaravelAcl\Authentication\Models;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;

class Media extends Model {

    protected $table = 'content_images';
    protected $fillable = ['user_id', 'image_title', 'tag', 'image'];

    /**
     * Get the Category name .
     */
    public function user() {
        return $this->belongsTo('LaravelAcl\Authentication\Models\User');
    }

}
