<?php
namespace App;
namespace LaravelAcl\Authentication\Models; 

use Illuminate\Database\Eloquent\Model;
use LaravelAcl\Authentication\Models\Tag;
use Cartalyst\Sentry\Users\LoginRequiredException;
use App\Campaign;

class Snippet extends Model
{
    protected $fillable = ['description', 'tag_id', 'campaign_id','thumbnail', 'verticles', 'title']; 
    
     /**
     * Get the Category name .
     */
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
    
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
    
}