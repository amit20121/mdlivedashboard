<?php  namespace LaravelAcl\Authentication\Models;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentry\Users\LoginRequiredException;

class Tag extends Model
{
    protected $fillable = ['tag_name'];
    
    /**
     * Get all of the templates for the tag.
     */
    public function templates()
    {
        return $this->hasMany(Template::class);
    }
    /**
     * Get all of the templates for the category.
     */
    public function contents()
    {
        return $this->hasMany(Content::class);
    }
}
