<?php
namespace App;
namespace LaravelAcl\Authentication\Models; 

use Illuminate\Database\Eloquent\Model;
use LaravelAcl\Authentication\Models\Tag;
use Cartalyst\Sentry\Users\LoginRequiredException;
use App\Campaign;

class Content extends Model
{
    protected $fillable = ['description', 'tag_id', 'campaign_id','content_title','thumbnail', 'verticles', 'saveas', 'downloadable_content','default']; 
    
     /**
     * Get the Tag name
     */
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
    
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }
    
}
