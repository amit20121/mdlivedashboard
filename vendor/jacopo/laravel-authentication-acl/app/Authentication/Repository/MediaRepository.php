<?php

namespace LaravelAcl\Authentication\Repository;

/**
 * Class ContentRepository
 *
 * @author Sahbaj Uddin
 */
use Illuminate\Database\Eloquent\ModelNotFoundException;
use LaravelAcl\Library\Repository\Interfaces\BaseRepositoryInterface;
use LaravelAcl\Authentication\Models\Media;
use LaravelAcl\Authentication\Exceptions\UserNotFoundException as NotFoundException;
use App,
    Event;
use Illuminate\Support\Facades\Config;

class MediaRepository {

    /**
     * Sentry instance
     * @var
     */
    public function __construct() {
        //$this->sentry = App::make('sentry');
        //$this->config_reader = $config_reader ? $config_reader : App::make('config');
        //return parent::__construct(new Category);
    }

    /**
     * Create a new object
     *
     * @return mixed
     * @override
     */
    public function create(array $input) {
        $data = array(
            "user_id" => $input["user_id"],
            "image_title" => $input["image_title"],
            "tag" => $input["tag"],
            "image" => $input["image"]
        );
        try {
            $media = new Media;
            $media->create($data);
        } catch (CartaUserExists $e) {
            throw new NotFoundException;
        }

        return $media;
    }

    public function all(array $search_filters = [], $verticles = null) {
        $q = new Media;
        $per_page = Config::get('acl_base.contents_per_page');
        if (empty($verticles))
            return $q->orderBy('id', 'DESC')->paginate($per_page);
        else
            return $q->where('verticles', $verticles)->orderBy('id', 'DESC')->paginate($per_page);
    }

    /**
     * Update a new object
     *
     * @param       id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data) {
        $obj = $this->find($id);
        Event::fire('repository.updating', [$obj]);
        $obj->update($data);
        return $obj;
    }

    /**
     * Obtains all models
     *
     * @override
     * @param array $search_filters
     * @return mixed
     */
    public function delete($id) {
        $obj = $this->find($id);
        return $obj->delete();
    }

    /**
     * Find a model by his id
     *
     * @param $id
     * @return mixed
     * @throws \LaravelAcl\Authentication\Exceptions\UserNotFoundException
     */
    public function find($id) {
        try {
            $media = Media::find($id);
        } catch (GroupNotFoundException $e) {
            throw new NotFoundException;
        }

        return $media;
    }

}
