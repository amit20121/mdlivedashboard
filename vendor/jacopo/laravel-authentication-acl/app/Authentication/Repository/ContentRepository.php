<?php

namespace LaravelAcl\Authentication\Repository;

/**
 * Class ContentRepository
 *
 * @author Sahbaj Uddin
 */
use Illuminate\Database\Eloquent\ModelNotFoundException;
use LaravelAcl\Library\Repository\Interfaces\BaseRepositoryInterface;
use LaravelAcl\Authentication\Models\Content;
use LaravelAcl\Authentication\Exceptions\UserNotFoundException as NotFoundException;
use App,
    Event;
use Illuminate\Support\Facades\Config;

class ContentRepository {

    /**
     * Sentry instance
     * @var
     */
    public function __construct() {
        //$this->sentry = App::make('sentry');
        //$this->config_reader = $config_reader ? $config_reader : App::make('config');
        //return parent::__construct(new Category);
    }

    /**
     * Create a new object
     *
     * @return mixed
     * @override
     */
    public function create(array $input) {

        $data = array(
            "description" => $input["description"],
            "tag_id" => $input["tag_id"],
            "content_title" => $input["content_title"],
            "campaign_id" => $input["campaign_id"],
            "thumbnail" => $input["thumbnail"],
            "default" => $input["default"],
        );

        try {
            //$category = $this->sentry->createCategory($data);
            $content = new Content;
            $content->create($data);
        } catch (CartaUserExists $e) {
            throw new NotFoundException;
        }

        return $content;
    }

    /**
     * Update a new object
     *
     * @param       id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data) {
        $obj = $this->find($id);
        Event::fire('repository.updating', [$obj]);
        $obj->update($data);
        return $obj;
    }

    /**
     * Obtains all models
     *
     * @override
     * @param array $search_filters
     * @return mixed
     */
    public function all(array $search_filters = [], $verticles = null) {
        $q = new Content;
        $per_page = Config::get('acl_base.contents_per_page');
        if (empty($verticles))
            return $q->orderBy('id','desc')->paginate($per_page);
        else
            return $q->where('verticles', $verticles)
                            ->where("default", 1)
                            ->orderBy('id','desc')
                            ->paginate($per_page);
    }

    /**
     * @param array $search_filters
     * @param       $q
     * @return mixed
     */
    protected function applySearchFilters(array $search_filters, $q) {
        if (isset($search_filters['tag_name']) && $search_filters['tag_name'] !== '')
            $q = $q->where('tag_name', 'LIKE', "%{$search_filters['tag_name']}%");
        return $q;
    }

    /**
     * Deletes a new object
     *
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        $obj = $this->find($id);
        Event::fire('repository.deleting', [$obj]);
        return $obj->delete();
    }

    /**
     * Find a model by his id
     *
     * @param $id
     * @return mixed
     * @throws \LaravelAcl\Authentication\Exceptions\UserNotFoundException
     */
    public function find($id) {
        try {
            //$flight = App\Flight::find(1);
            $content = Content::find($id);
            //$category = $this->sentry->findGroupById($id);
        } catch (GroupNotFoundException $e) {
            throw new NotFoundException;
        }

        return $content;
    }
    public function findLeftContent(array $input) {
        try {
            //$flight = App\Flight::find(1);
            $content = Content::find($id);
            //$category = $this->sentry->findGroupById($id);
        } catch (GroupNotFoundException $e) {
            throw new NotFoundException;
        }

        return $content;
    }

}
