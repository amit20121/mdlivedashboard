<?php

namespace App\Http\Controllers;

namespace LaravelAcl\Authentication\Controllers;

/**
 * Class ContentController
 *
 * @author Sahbaj Uddin
 */
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use LaravelAcl\Library\Form\FormModel;
use LaravelAcl\Authentication\Helpers\FormHelper;
use LaravelAcl\Authentication\Models\Content;
use LaravelAcl\Authentication\Models\Media;
use LaravelAcl\Authentication\Exceptions\UserNotFoundException;
use LaravelAcl\Library\Validators\ValidatorInterface;
use LaravelAcl\Authentication\Repository\MediaRepository;
use LaravelAcl\Library\Exceptions\JacopoExceptionsInterface;
use View,
    Redirect,
    App,
    Config,
    URL;
use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;

class MediaController extends Controller {

    /**
     * @var use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;
     */
    protected $media;
    protected $auth;

    public function __construct(MediaRepository $media) {
        $this->media = $media;
        $this->sidebar = array(
            "Media List" => array('url' => route("media.getList"), 'icon' => '<i class="glyphicon glyphicon-th-list"></i>'),
            'Add New' => array('url' => route('contents.getImages'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );
    }

    public function contentImages() {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        return View::make('laravel-authentication-acl::admin.media.content-images')->with(['sidebar_items' => $this->sidebar]);
    }

    public function getList(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        if ($user_group == 'admin' || $user_group == "superadmin") {
            $medias = $this->media->all($request->except(['page']));
            return View::make('laravel-authentication-acl::admin.media.view')->with(['medias' => $medias, "request" => $request, 'sidebar_items' => $this->sidebar]);
        } else {
            return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.not_authorized'));
        }
    }

    public function viewMedia(Request $request) {
        $obj = $this->media->find($request->get('id'));
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        if ($user_group == 'admin' || $user_group == "superadmin") {
            return View::make('laravel-authentication-acl::admin.media.view-media')->with(["media" => $obj, 'sidebar_items' => $this->sidebar]);
        } else {
            return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.not_authorized'));
        }
    }

    public function uploadImages(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $this->validate($request, [
            'image' => 'required|image'
        ]);

        $files = $request->image;
        $extension = $files->getClientOriginalExtension();
        $name = $files->getClientOriginalName();

        $media = new Media;
        if (isset($request->image_title) && !empty($request->image_title)) {
            $image_title = $request->image_title;
        } else {
            $image_title = str_replace("." . $extension, '', $name);
        }
        $file_name = $image_title . date('His') . "." . $extension;
        $submit_return = $this->media->create([
            'user_id' => $user->id,
            'tag' => $request->tag,
            'image_title' => $image_title,
            'image' => $image_title . date('His') . "." . $extension,
        ]);

        if ($submit_return == TRUE) {
            $destinationPath = base_path() . '/public/content_builder_images';
            $files->move($destinationPath, $file_name);
        }
        return Redirect::route('contents.getImages')->withMessage(Config::get('acl_messages.flash.success.media_success'));
    }

    public function updateMedia(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $input = $request->except(['_token']);
        if (isset($input['old_image']) && empty($input['old_image'])) {
            $this->validate($request, [
                'image' => 'required|image'
            ]);
        }
        if (isset($input['image']) && !empty($input['image'])) {
            $files = $request->image;
            $extension = $files->getClientOriginalExtension();
            $name = $files->getClientOriginalName();
            if (file_exists(base_path() . '/public/content_builder_images/' . $input['old_image'])) {
                unlink(base_path() . '/public/content_builder_images/' . $input['old_image']);
            }
        } else {
            $name = $input['old_image'];
        }

        $media = new Media;
        if (isset($request->image_title) && !empty($request->image_title)) {
            $image_title = $request->image_title;
        } else {
            $image_title = str_replace("." . $extension, '', $name);
        }
        if (isset($input['image']) && !empty($input['image'])) {
            $image = $image_title . date('His') . "." . $extension;
        } else {
            $image = $input['old_image'];
        }
        Media::where('id', $request->id)
                ->update([
                    'image_title' => $request->image_title,
                    'tag' => $request->tag,
                    'image' => $image
        ]);

        if (isset($input['image']) && !empty($input['image'])) {
            $destinationPath = base_path() . '/public/content_builder_images';
            $files->move($destinationPath, $image_title . date('His') . "." . $extension);
        }
        return Redirect::route('media.getList')->withMessage(Config::get('acl_messages.flash.success.media_success'));
    }

    public function deleteMedia(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        if ($user_group == 'admin' || $user_group == "superadmin") {
            $media = $this->media->find($request->get('id'));
            $this->media->delete($request->get('id'));
            if (isset($media->image) && file_exists(base_path() . '/public/content_builder_images/' . $media->image)) {
                unlink(base_path() . '/public/content_builder_images/' . $media->image);
            }
            return Redirect::route('media.getList')->withMessage(Config::get('acl_messages.flash.success.media_delete'));
        } else {
            return Redirect::route('templates.list')->withMessage(Config::get('acl_messages.flash.success.not_authorized'));
        }
    }
}
