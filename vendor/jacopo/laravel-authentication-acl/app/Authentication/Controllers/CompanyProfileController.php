<?php

namespace LaravelAcl\Authentication\Controllers;

/**
 * Class UserController
 *
 * @author jacopo beschi jacopo@jacopobeschi.com
 */
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use LaravelAcl\Authentication\Exceptions\PermissionException;
use LaravelAcl\Authentication\Exceptions\ProfileNotFoundException;
use LaravelAcl\Authentication\Helpers\DbHelper;
use LaravelAcl\Authentication\Models\UserProfile;
use LaravelAcl\Authentication\Presenters\UserPresenter;
use LaravelAcl\Authentication\Services\UserProfileService;
use LaravelAcl\Authentication\Validators\UserProfileAvatarValidator;
use LaravelAcl\Library\Exceptions\NotFoundException;
use LaravelAcl\Authentication\Models\User;
use LaravelAcl\Authentication\Helpers\FormHelper;
use LaravelAcl\Authentication\Exceptions\UserNotFoundException;
use LaravelAcl\Authentication\Validators\UserValidator;
use LaravelAcl\Library\Exceptions\JacopoExceptionsInterface;
use LaravelAcl\Authentication\Validators\UserProfileValidator;
use View,
    Redirect,
    App,
    Config;
use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CompanyProfileController extends Controller {

    /**
     * @var \LaravelAcl\Authentication\Repository\SentryUserRepository
     */
    protected $user_repository;
    protected $user_validator;

    /**
     * @var \LaravelAcl\Authentication\Helpers\FormHelper
     */
    protected $form_helper;
    protected $profile_repository;
    protected $profile_validator;

    /**
     * @var use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;
     */
    protected $auth;
    protected $register_service;
    protected $custom_profile_repository;

    public function __construct(UserValidator $v, FormHelper $fh, UserProfileValidator $vp, AuthenticateInterface $auth) {
        $this->user_repository = App::make('user_repository');
        $this->user_validator = $v;
        $this->f = App::make('form_model', [$this->user_validator, $this->user_repository]);
        $this->form_helper = $fh;
        $this->profile_validator = $vp;
        $this->profile_repository = App::make('profile_repository');
        $this->auth = $auth;
        $this->register_service = App::make('register_service');
        $this->custom_profile_repository = App::make('custom_profile_repository');
    }

    public function companyProfile(Request $request) {
        $logged_user = $this->auth->getLoggedUser();
        $custom_profile_repo = App::make('custom_profile_repository', [$logged_user->user_profile()->first()->id]);
        return View::make('laravel-authentication-acl::admin.user.company-profile')
                        ->with([
                            "user_profile" => $logged_user->user_profile()
                            ->first(),
                            "custom_profile" => $custom_profile_repo
        ]);
    }

    public function EditCompanyProfile(Request $request) {
        $input = $request->all();
        $input['company_name'] = (isset($input['company_name']) && !empty($input['company_name'])) ? array_filter($input['company_name']) : "";
        $input['urls'] = (isset($input['urls']) && !empty($input['urls'])) ? array_filter($input['urls']) : "";
        $input['copay_statement'] = (isset($input['copay_statement']) && !empty($input['copay_statement'])) ? array_filter($input['copay_statement']) : "";
        $input['monikers'] = (isset($input['monikers']) && !empty($input['monikers'])) ? array_filter($input['monikers']) : "";
        if (empty($input['company_name'])) {
            unset($input['company_name']);
        } else {
            if (isset($input['company_name_default']) && !empty($input['company_name_default'])) {
                $input['company_name']['Default'] = $input['company_name_default'];
                $input['company_name'] = json_encode($input['company_name']);
            } else {
                $input['company_name']['Default'] = "";
                $input['company_name'] = json_encode($input['company_name']);
            }
        }
        if (empty($input['urls'])) {
            unset($input['urls']);
        } else {
            if (isset($input['urls_default']) && !empty($input['urls_default'])) {
                $input['urls']['Default'] = $input['urls_default'];
                $input['urls'] = json_encode($input['urls']);
            } else {
                $input['urls']['Default'] = "";
                $input['urls'] = json_encode($input['urls']);
            }
        }
        if (empty($input['copay_statement'])) {
            unset($input['copay_statement']);
        } else {
            if (isset($input['copay_statement_default']) && !empty($input['copay_statement_default'])) {
                $input['copay_statement']['Default'] = $input['copay_statement_default'];
                $input['copay_statement'] = json_encode($input['copay_statement']);
            } else {
                $input['copay_statement']['Default'] = "";
                $input['copay_statement'] = json_encode($input['copay_statement']);
            }
        }
        if (empty($input['monikers'])) {
            unset($input['monikers']);
        } else {
            if (isset($input['monikers_default']) && !empty($input['monikers_default'])) {
                $input['monikers']['Default'] = $input['monikers_default'];
                $input['monikers'] = json_encode($input['monikers']);
            } else {
                $input['monikers']['Default'] = "";
                $input['monikers'] = json_encode($input['monikers']);
            }
        }
        $service = new UserProfileService($this->profile_validator);
        try {
            $service->processForm($input);
        } catch (JacopoExceptionsInterface $e) {
            $errors = $service->getErrors();
            return Redirect::back()
                            ->withInput()
                            ->withErrors($errors);
        }
        return Redirect::back()
                        ->withInput()
                        ->withMessage(Config::get('acl_messages.flash.success.user_profile_edit_success'));
    }

    public function deleteProfile(Request $request) {
        $input = $request->except('_token');
        UserProfile::deleteProfile($input);
        echo TRUE;
    }

}
