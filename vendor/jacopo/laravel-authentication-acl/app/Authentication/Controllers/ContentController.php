<?php

namespace App\Http\Controllers;

namespace LaravelAcl\Authentication\Controllers;

/**
 * Class ContentController
 *
 * @author Sahbaj Uddin
 */
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use LaravelAcl\Library\Form\FormModel;
use LaravelAcl\Authentication\Helpers\FormHelper;
use LaravelAcl\Authentication\Models\Content;
use LaravelAcl\Authentication\Exceptions\UserNotFoundException;
use LaravelAcl\Library\Validators\ValidatorInterface;
use LaravelAcl\Authentication\Validators\ContentValidator;
use LaravelAcl\Authentication\Repository\ContentRepository;
use LaravelAcl\Authentication\Repository\SentryTagRepository;
use LaravelAcl\Library\Exceptions\JacopoExceptionsInterface;
use View,
    Redirect,
    App,
    Config,
    Storage,
    File,
    DB,
    Response;

//use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;


class ContentController extends Controller {

    /**
     * @var \LaravelAcl\Authentication\Repository\ContentRepository
     */
    protected $contents;
    protected $content_validator;

    /**
     * @var \LaravelAcl\Authentication\Helpers\FormHelper
     */
    protected $form_helper;

    /**
     * @var use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;
     */
    protected $auth;

    public function __construct(ContentValidator $v, FormHelper $fh, ContentRepository $contents) {
        $this->contents = $contents;
        $this->content_validator = $v;
        $this->f = App::make('form_model', [$this->content_validator, $this->contents]);
        $this->form_helper = $fh;

        $this->sidebar = array(
            "Content List" => array('url' => route('contents.list'), 'icon' => '<i class="glyphicon glyphicon-th-list"></i>'),
            'Add New' => array('url' => route('contents.new'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );
    }

    public function getList(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        if ($user_group == "admin" || $user_group == "superadmin") {
            $contents = $this->contents->all($request->except(['page']));
            return View::make('laravel-authentication-acl::admin.contents.index')->with(["contents" => $contents, "request" => $request]);
        } else {
            if ($user_group == "health system admin")
                $contents = $this->contents->all($request->except(['page']), "health_system");
            elseif ($user_group == "health plan admin")
                $contents = $this->contents->all($request->except(['page']), "health_plan");
            elseif ($user_group == "employer admin")
                $contents = $this->contents->all($request->except(['page']), "employer");
            else
                $contents = $this->contents->all($request->except(['page']), $user_group);
            return View::make('laravel-authentication-acl::admin.contents.index-verticles')->with(["contents" => $contents, "request" => $request]);
        }
    }

    /**
     * Destroy the given content.
     *
     * @param  Request  $request
     * @param  Content  $content
     * @return Response
     */
    public function deleteContent(Request $request) {
        $this->contents->delete($request->get('id'));
        return Redirect::route('contents.list')->withMessage(Config::get('acl_messages.flash.success.content_delete_success'));
    }

    public function editContent(Request $request) {
        try {
            $obj = $this->contents->find($request->get('id'));
        } catch (UserNotFoundException $e) {
            $obj = new Content;
        }
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);

        return View::make('laravel-authentication-acl::admin.contents.edit')->with(["content" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group]);
    }

    public function newContent(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        return View::make('laravel-authentication-acl::admin.contents.new')->with(['sidebar_items' => $this->sidebar, "user_group" => $user_group]);
    }

    public function postEditContent(Request $request) {
        $id = $request->get('id');

        try {

            $input = $request->except('_token');
            if (!isset($input['default'])) {
                $input['default'] = '0';
            }
       /*     
            if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
                $files = $input['thumbnail'];
                $extension = $files->getClientOriginalExtension();
                $picture = date('His') . "." . $extension;
                $input['thumbnail'] = $picture;
                $destinationPath = base_path() . '/public/thumbnail';
                if (isset($input['old_thumbnail']) && !empty($input['old_thumbnail']) && file_exists(base_path() . '/public/thumbnail/' . $input['old_thumbnail'])) {
                    unlink(base_path() . '/public/thumbnail/' . $input['old_thumbnail']);
                }
            } else {
                unset($input['thumbnail']);
            }
            unset($input['old_thumbnail']);
            */
            $obj = $this->f->process($input);
/*
            if ($obj) {
                if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
                    $files->move($destinationPath, $picture);
                }
*/
                if (isset($input['downloadable_content']) && !empty($input['downloadable_content'])) {
                    $uploaded_file = $input['downloadable_content'];
                    $filename = $uploaded_file->getClientOriginalName();
                    $destination_path = "downloadable_content/" . $obj->id . "/";
                    $exists = Storage::disk('local')->exists($destination_path . $input['old_downloadable_content']);
                    //$input['downloadable_content'] = $filename;
                    if (isset($input['old_downloadable_content']) && !empty($input['old_downloadable_content']) && $exists) {
                        Storage::delete($destination_path . $input['old_downloadable_content']);
                    }

                    $bytes_written = Storage::disk('local')->put($destination_path . $filename, File::get($uploaded_file));

                    if ($bytes_written === false) {
                        return Redirect::route('contents.edit', ["id" => $obj->id])->withMessage(Config::get('acl_messages.flash.error.content_file_upload_error'));
                    } else {
                        Content::where('id', $obj->id)
                                ->update(['downloadable_content' => $filename]);
                    }
                } else {
                    unset($input['downloadable_content']);
                }
                unset($input['old_downloadable_content']);
           // }
            
        } catch (JacopoExceptionsInterface $e) {
            $errors = $this->f->getErrors();
            // passing the id incase fails editing an already existing item
            return Redirect::route("contents.edit", $id ? ["id" => $id] : [])->withInput()->withErrors($errors);
        }
         //\Alert::success(Config::get('acl_messages.flash.success.content_edit_success'))->flash();
        
        //return Redirect::route('contents.edit', ["id" => $obj->id]);
        //return Redirect::route('contents.edit', ["id" => $obj->id])->withMessage(Config::get('acl_messages.flash.success.content_edit_success'));
         return Redirect::route('contents.list')->withMessage(Config::get('acl_messages.flash.success.content_edit_success'));
    }

    public function postContent(Request $request) {
        $this->validate($request, [
            'content_title' => 'required',
            'description' => 'required'
        ]);

        try {
            $input = $request->except('_token');
            /*
            if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
                $files = $input['thumbnail'];
                $extension = $files->getClientOriginalExtension();
                $picture = date('His') . "." . $extension;
                $input['thumbnail'] = $picture;
                $destinationPath = base_path() . '/public/thumbnail';
            } else {
                $input['thumbnail'] = "";
            }
            */

            $data = new Content;

            $data->tag_id = $input['tag_id'];
            $data->campaign_id = $input['campaign_id'];
            $data->content_title = $input['content_title'];
            $data->description = $input['description'];
            $data->verticles = $input['verticles'];
            $data->thumbnail = $input['thumbnail'];
            if (!isset($input['default'])) {
                $input['default'] = '0';
            }
            $data->default = $input['default'];


            if ($data->save()) {

                //$obj = $this->f->process($input);
                //if ($obj) {
                //dd($data->id);
            /*    if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
                    $files->move($destinationPath, $picture);
                }
             */
                if (isset($input['downloadable_content']) && !empty($input['downloadable_content'])) {
                    $uploaded_file = $input['downloadable_content'];
                    $filename = $uploaded_file->getClientOriginalName();
                    $destination_path = "downloadable_content/" . $data->id . "/";

                    $bytes_written = Storage::disk('local')->put($destination_path . $filename, File::get($uploaded_file));

                    if ($bytes_written === false) {
                        return Redirect::route('contents.edit', ["id" => $data->id])->withMessage(Config::get('acl_messages.flash.error.content_file_upload_error'));
                    } else {

                        Content::where('id', $data->id)
                                ->update(['downloadable_content' => $filename]);
                    }
                } else {
                    unset($input['downloadable_content']);
                }
            }
        } catch (JacopoExceptionsInterface $e) {
            $errors = $this->f->getErrors();
            // passing the id incase fails editing an already existing item
            return Redirect::route("contents.new", [])->withInput()->withErrors($errors);
        }
        return Redirect::route('contents.list')->withMessage(Config::get('acl_messages.flash.success.content_new_success'));
    }

    public function downloadContent(Request $request) {
        $obj = $this->contents->find($request->get('id'));
        return Response::download(base_path() . "/storage/app/downloadable_content/" . $obj->id . "/" . $obj->downloadable_content);
    }

    public function viewLeftSideBarContent(Request $request) {
        $input = $request->except('_token');
        
        if (isset($input['content_id']) && !empty($input['content_id']) && $input['content_id'] != '0') {
            $contents = Content::where("tag_id", $input['tag_id'])
                    ->where("campaign_id", $input['content_id'])
                    ->where("verticles", $input['verticles'])
                    ->first();
            if (isset($contents) && !empty($contents)) {
                $content = $contents->description;
            } else {
                $content = "";
            }
        } else if ($input['content_id'] == '0' && $input['action'] == "edit") {
            $contents = App\Template::where("id", $input['c_id'])
                    ->first();
            if (isset($contents) && !empty($contents)) {
                $content = $contents->template_content;
            } else {
                $content = "";
            }
        } else {
            $contents = Content::where("id", $input['c_id'])
                    ->first();
            if (isset($contents) && !empty($contents)) {
                $content = $contents->description;
            } else {
                $content = "";
            }
        }
        return View::make('laravel-authentication-acl::admin.contents.left-content')->with(["contents" => $content]);
    }

}
