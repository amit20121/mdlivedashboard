<?php

namespace App\Http\Controllers;

namespace LaravelAcl\Authentication\Controllers;

/**
 * Class SnippetController
 *  */
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use LaravelAcl\Library\Form\FormModel;
use LaravelAcl\Authentication\Helpers\FormHelper;
use LaravelAcl\Authentication\Models\Snippet;
use LaravelAcl\Authentication\Exceptions\UserNotFoundException;
use LaravelAcl\Library\Validators\ValidatorInterface;
use LaravelAcl\Authentication\Validators\SnippetValidator;
use LaravelAcl\Authentication\Helpers\simple_html_dom;
use LaravelAcl\Authentication\Repository\SnippetRepository;
use LaravelAcl\Authentication\Repository\SentrySnippetRepository;
use LaravelAcl\Library\Exceptions\JacopoExceptionsInterface;
use View,
    Redirect,
    App,
    Config,
    Storage,
    File,
    DB,
    Illuminate\Support\Facades\URL,
    Response;

//use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;

class SnippetController extends Controller {

    /**
     * @var \LaravelAcl\Authentication\Repository\SnippetRepository
     */
    protected $snippet;
    protected $snippet_validator;

    /**
     * @var \LaravelAcl\Authentication\Helpers\FormHelper
     */
    protected $form_helper;

    /**
     * @var use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;
     */
    protected $auth;
    protected $Snippetpath;

    public function __construct(SnippetValidator $v, FormHelper $fh, SnippetRepository $snippet) {
        $this->snippet = $snippet;
        $this->snippet_validator = $v;
        $this->f = App::make('form_model', [$this->snippet_validator, $this->snippet]);
        $this->form_helper = $fh;
        $this->Snippetpath = base_path() . '/public/vendor/content-builder/assets/minimalist-basic/';

        $this->sidebar = array(
            "Snippet List" => array('url' => route('snippets.list'), 'icon' => '<i class="glyphicon glyphicon-th-list"></i>'),
            'Add New' => array('url' => route('snippets.new'), 'icon' => '<i class="fa fa-plus-circle"></i>'),
        );
    }

    public function getList(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);

        if ($user_group == "admin" || $user_group == "superadmin") {
            $snippet = $this->snippet->all($request->except(['page']));

            return View::make('laravel-authentication-acl::admin.snippets.index')->with(["contents" => $snippet, "request" => $request]);
        } else {
            if ($user_group == "health system admin")
                $contents = $this->snippet->all($request->except(['page']), "health_system");
            elseif ($user_group == "health plan admin")
                $contents = $this->snippet->all($request->except(['page']), "health_plan");
            elseif ($user_group == "employer admin")
                $contents = $this->snippet->all($request->except(['page']), "employer");
            else
                $contents = $this->snippet->all($request->except(['page']), $user_group);
            return View::make('laravel-authentication-acl::admin.snippets.index-verticles')->with(["contents" => $contents, "request" => $request]);
        }
    }

    public function deleteSnippet(Request $request) {
        $id = $request->get('id');
        $data = Snippet::find($id);
        $tag_id = $data->tag_id;

        if ($this->snippet->delete($id, $tag_id)) {
            $snippetFile = $this->Snippetpath . $this->getSnippetFile($data->verticles, $tag_id);

            if ($this->deletesnippethtml($id, $snippetFile) == false) {
                return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_delete_without_html_success'));
            }
        }

        return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_delete_success'));
    }

    public function editSnippet(Request $request) {
        try {
            $obj = $this->snippet->find($request->get('id'));
        } catch (UserNotFoundException $e) {
            $obj = new Snippet;
        }
        $campaign_id = explode(",", $obj->campaign_id);
        $obj->campaign_id = $campaign_id;
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        return View::make('laravel-authentication-acl::admin.snippets.edit')->with(["content" => $obj, 'sidebar_items' => $this->sidebar, "user_group" => $user_group]);
    }

    public function newSnippet(Request $request) {
        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);
        return View::make('laravel-authentication-acl::admin.snippets.new')->with(['sidebar_items' => $this->sidebar, "user_group" => $user_group,]);
    }

    public function postEditSnippet(Request $request) {
        $this->validate($request, [
            'description' => 'required', 'campaign_id' => 'required', 'thumbnail' => 'required', 'title' => 'required'
        ]);

        $id = $request->get('id');
        $data = Snippet::find($id);

        try {
            $input = $request->except('_token');
            $thumbnail = $input['thumbnail'];
            /*        if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
              $files = $input['thumbnail'];
              $extension = $files->getClientOriginalExtension();
              $picture = time() . "." . $extension;
              $input['thumbnail'] = $picture;
              $destinationPath = base_path() . '/public/vendor/content-builder/assets/minimalist-basic/thumbnails';
              if (isset($input['old_thumbnail']) && !empty($input['old_thumbnail']) && file_exists(base_path() . '/public/thumbnail/' . $input['old_thumbnail'])) {
              unlink(base_path() . '/public/vendor/content-builder/assets/minimalist-basic/thumbnails' . $input['old_thumbnail']);
              }
              } else {
              unset($input['thumbnail']);
              }unset($data['old_thumbnail']);
             */

            $campaignArr = implode(",", $request->campaign_id);
            $input['campaign_id'] = $campaignArr;
            $data = $input;
            unset($data['Cverticles']);
            unset($data['Ctag_id']);
            $obj = $this->f->process($data);
            $tag_id = $input['tag_id'];
            $title = $input['title'];
            /*
              if ($obj) {
              if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
              $files->move($destinationPath, $picture);
              }
              }
             */
            /* Editing Snippet html file */
            //         if (empty($picture)) {
            $picture = $thumbnail;
            //        }

            $campaign_id = $input['campaign_id'];
            $description = $input['description'];

            /* For If the verticles and tag_id are changed */
            if ($input['verticles'] != $input['Cverticles'] && $input['tag_id'] != $input['Ctag_id']) {
                $snippetFile = $this->Snippetpath . $this->getSnippetFile($input['Cverticles'], $input['Ctag_id']);
                if ($this->deletesnippethtml($id, $snippetFile) == false) {
                    return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_update_without_html_success'));
                }

                $snippetFile = $this->Snippetpath . $this->getSnippetFile($input['verticles'], $input['tag_id']);
                $this->UpdateSnipperIntoHtml($id, $snippetFile, $thumbnail, $campaign_id, $description, $title);
            } elseif ($input['verticles'] != $input['Cverticles']) {
                $this->editOldSnippet($id, $input['Cverticles'], $input['verticles'], $picture, $campaign_id, $description, $tag_id, $title);
            } elseif ($input['tag_id'] != $input['Ctag_id']) {
                if ($tag_id == 4 || $tag_id == 11) {
                    $this->editOldSnippetwithTag_id($id, $input['Cverticles'], $input['verticles'], $picture, $campaign_id, $description, $tag_id, $input['Ctag_id'], $title);
                } elseif ($input['Ctag_id'] == 4 || $input['Ctag_id'] == 11) {
                    $this->editOldSnippetwithCTag_id($id, $input['Cverticles'], $input['verticles'], $picture, $campaign_id, $description, $tag_id, $input['Ctag_id'], $title);
                } else {

                    $snippetFile = $this->Snippetpath . $this->getSnippetFile($input['verticles'], $tag_id);
                    $snipperId = "snipper-$id";
                    $html = "<div id=\"$snipperId\" data-title=\"$title\" data-thumb=\"/$picture\" data-cat=\"0,$campaign_id\">";
                    $finalhtml = $html . $description . "</div>";
                    $simObj = new simple_html_dom();
                    $simObj->load_file($snippetFile);
                    $SnippetPortion = $simObj->find("div[id=$snipperId]", 0);
                    if (isset($SnippetPortion) && !empty($SnippetPortion)) {
                        $SnippetPortion->outertext = $finalhtml;
                        $simObj->save($snippetFile);
                    } else {
                        return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_update_without_html_success'));
                    }
                }
            } else {

                $snippetFile = $this->Snippetpath . $this->getSnippetFile($input['verticles'], $tag_id);
                $snipperId = "snipper-$id";
                $html = "<div id=\"$snipperId\" data-title=\"$title\" data-thumb=\"/$picture\" data-cat=\"0,$campaign_id\">";
                $finalhtml = $html . $description . "</div>";
                $simObj = new simple_html_dom();
                $simObj->load_file($snippetFile);
                $SnippetPortion = $simObj->find("div[id=$snipperId]", 0);
                if (isset($SnippetPortion) && !empty($SnippetPortion)) {
                    $SnippetPortion->outertext = $finalhtml;
                    $simObj->save($snippetFile);
                } else {
                    return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_update_without_html_success'));
                }
            }
        } catch (JacopoExceptionsInterface $e) {
            $errors = $this->f->getErrors();
            // passing the id incase fails editing an already existing item
            return Redirect::route("snippets.edit", $id ? ["id" => $id] : [])->withInput()->withErrors($errors);
        }
        //\Alert::success(Config::get('acl_messages.flash.success.snippet_edit_success'))->flash();

        // return Redirect::route('snippets.edit', ["id" => $obj->id]);
        return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_edit_success'));
    }

    public function postSnippet(Request $request) {

        $this->validate($request, [
            'description' => 'required', 'thumbnail' => 'required', 'campaign_id' => 'required', 'title' => 'required'
        ]);

        $campaignArr = implode(",", $request->campaign_id);
        $request->campaign_id = $campaignArr;
        $request->merge(array('campaign_id' => $campaignArr));

        $authentication = \App::make('authenticator');
        $user = $authentication->getLoggedUser();
        $user_group = strtolower($user->groups()->first()->name);

        try {
            $input = $request->except('_token');
            /*     if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
              $files = $input['thumbnail'];
              $extension = $files->getClientOriginalExtension();
              $picture = time() . "." . $extension;
              $input['thumbnail'] = $picture;
              $destinationPath = base_path() . '/public/vendor/content-builder/assets/minimalist-basic/thumbnails/';
              } else {
              $input['thumbnail'] = "";
              }
             */
            $data = new Snippet;
            $data->tag_id = $input['tag_id'];
            $data->campaign_id = $input['campaign_id'];
            $data->description = $input['description'];
            $data->verticles = $input['verticles'];
            $data->thumbnail = $input['thumbnail'];
            $data->title = $input['title'];

            if ($data->save()) {
                /*
                  if (isset($input['thumbnail']) && !empty($input['thumbnail'])) {
                  $files->move($destinationPath, $picture);
                  }
                 */
                /* Adding snipper into snipper html template file */
                $this->addSnipperIntoHtml($data->id, $input['verticles'], $data->thumbnail, $data->campaign_id, $data->description, $data->tag_id, $input['title']);
            }
        } catch (JacopoExceptionsInterface $e) {
            $errors = $this->f->getErrors();
            // passing the id incase fails editing an already existing item
            return Redirect::route("snippets.new", [])->withInput()->withErrors($errors);
        }
        return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_new_success'));
    }

    public function viewLeftSideBarSnippet(Request $request) {
        $input = $request->except('_token');
        if (isset($input['content_id']) && !empty($input['content_id']) && $input['content_id'] != '0') {
            $contents = Snippet::where("tag_id", $input['tag_id'])
                    ->where("campaign_id", $input['content_id'])
                    ->where("verticles", $input['verticles'])
                    ->first();
            if (isset($contents) && !empty($contents)) {
                $content = $contents->description;
            } else {
                $content = "";
            }
        } else if ($input['content_id'] == '0' && $input['action'] == "edit") {
            $contents = App\Template::where("id", $input['c_id'])
                    ->first();
            if (isset($contents) && !empty($contents)) {
                $content = $contents->template_content;
            } else {
                $content = "";
            }
        } else {
            $contents = Snippet::where("id", $input['c_id'])
                    ->first();
            if (isset($contents) && !empty($contents)) {
                $content = $contents->description;
            } else {
                $content = "";
            }
        }
        return View::make('laravel-authentication-acl::admin.snippets.left-snippet')->with(["contents" => $content]);
    }

    public function getGenerate(Request $request) {
        $snippetFile = $this->generateSelectVal();

        return View::make('laravel-authentication-acl::admin.snippets.snippet-generate')->with(["SnippetFileInfo" => $snippetFile]);
    }

    public function postGenerate(Request $request) {

        $SnippetRequestFileName = $request->snippetFile;
        if ($SnippetRequestFileName == "all") {
            if ($this->generateAllSnippet()) {
                return "All snippets has been re-generated successfully!!";
            };
        }

        $RequestFilewithFullpath = $this->Snippetpath . $SnippetRequestFileName . ".html";
        $RequestFile = @fopen($RequestFilewithFullpath, "r+");

        if ($RequestFile !== false) {
            ftruncate($RequestFile, 0);
            fclose($RequestFile);
        }

        $SnippetsDB = Snippet::all();
        foreach ($SnippetsDB as $snippet) {
            $id = $snippet->id;
            $tag_id = $snippet->tag_id;
            $verticles = $snippet->verticles;
            $thumbnail = $snippet->thumbnail;
            $campaign_id = $snippet->campaign_id;
            $title = $snippet->title;
            $description = $snippet->description;
            $DBSnippet = $this->getSnippetFile($verticles, $tag_id);

            if ($DBSnippet == $SnippetRequestFileName . ".html") {
                $this->GenerateSnipperIntoHtml($id, $RequestFilewithFullpath, $thumbnail, $campaign_id, $description, $tag_id, $title);
            }
        }
        //return View::make('laravel-authentication-acl::admin.snippets.snippet-generate')->with(["SnippetFileInfo" => $this->generateSelectVal()]);  
        return "Snippet has been re-generated successfully!!";
    }

    protected function generateAllSnippet() {

        $SnippetsDB = Snippet::all();

## Make Empty of all Snippets Files
        $SnippetFile = $this->generateSelectVal();
        unset($SnippetFile['all']);
        unset($SnippetFile['0']);
        $flat = call_user_func_array('array_merge', $SnippetFile);

## Write snippets all html template
        foreach (array_keys($flat) as $snipt) {
            $RequestFilewithFullpath = $this->Snippetpath . $snipt . ".html";

            $RequestFile = @fopen($RequestFilewithFullpath, "r+");
            if ($RequestFile !== false) {
                ftruncate($RequestFile, 0);
                fclose($RequestFile);
            }
        }

        foreach ($SnippetsDB as $snippet) {
            $id = $snippet->id;
            $tag_id = $snippet->tag_id;
            $verticles = $snippet->verticles;
            $thumbnail = $snippet->thumbnail;
            $campaign_id = $snippet->campaign_id;
            $description = $snippet->description;
            $title = $snippet->title;
            $DBSnippet = $this->getSnippetFile($verticles, $tag_id);
            $DBSnippetwithPath = $this->Snippetpath . $DBSnippet;

            $this->GenerateSnipperIntoHtml($id, $DBSnippetwithPath, $thumbnail, $campaign_id, $description, $tag_id, $title);
            
        }
    }

    protected function GenerateSnipperIntoHtml($id, $snippetFile, $thumbnail, $campaign_id, $description, $tag_id, $title) {
        $snipperId = "snipper-" . $id;
        $html = "<div id=\"$snipperId\" data-title=\"$title\" data-thumb=\"/$thumbnail\" data-cat=\"0,$campaign_id\">";
        $finalhtml = $html . $description . "</div>";
        $file = fopen($snippetFile, 'a');
        fwrite($file, "\r\n" . $finalhtml . "\r\n");
        fclose($file);
        
    }

    protected function generateSelectVal() {
        return array(
            "Health System" => array(
                'health_system_snippets' => 'Flyers',
                'health_system_html_email_snippets' => 'Html Email',
                'health_system_social_media_posts_snippets' => 'Social Media Posts',
                'health_system_posters_snippets' => 'Posters'),
            "Health Plan" => array(
                'health_plan_snippets' => 'Flyers',
                'health_plan_html_email_snippets' => 'Html Email',
                'health_plan_social_media_posts_snippets' => 'Social Media Posts',
                'health_plan_posters_snippets' => 'Posters'),
            "Employer" => array(
                'employer_snippets' => 'Flyers',
                'employer_html_email_snippets' => 'Html Email',
                'employer_social_media_posts_snippets' => 'Social Media Posts',
                'employer_posters_snippets' => 'Posters'),
            "All Snippets" => array(
                'all' => 'All Snippets'
            )
        );
    }

    /* Return Snippet Template File Name */

    protected function getSnippetFile($user_group, $tag_id) {

        if ($user_group == "health_system") {
            if ($tag_id == 4) {
                $SnippetFileName = "health_system_html_email_snippets.html";
            } elseif ($tag_id == 11) {
                $SnippetFileName = "health_system_social_media_posts_snippets.html";
            } elseif ($tag_id == 12) {
                $SnippetFileName = "health_system_posters_snippets.html";
            } else {
                $SnippetFileName = "health_system_snippets.html";
            }
        } elseif ($user_group == "health_plan") {
            if ($tag_id == 4) {
                $SnippetFileName = "health_plan_html_email_snippets.html";
            } elseif ($tag_id == 11) {
                $SnippetFileName = "health_plan_social_media_posts_snippets.html";
            } elseif ($tag_id == 12) {
                $SnippetFileName = "health_plan_posters_snippets.html";
            } else {
                $SnippetFileName = "health_plan_snippets.html";
            }
        } elseif ($user_group == "employer") {
            if ($tag_id == 4) {
                $SnippetFileName = "employer_html_email_snippets.html";
            } elseif ($tag_id == 11) {
                $SnippetFileName = "employer_social_media_posts_snippets.html";
            } elseif ($tag_id == 12) {
                $SnippetFileName = "employer_posters_snippets.html";
            } else {
                $SnippetFileName = "employer_snippets.html";
            }
        } else {
            $SnippetFileName = null;
        }

        if ($SnippetFileName == null) {
            return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_template_file_not_found'));
        } else
            return $SnippetFileName;
    }

    protected function addSnipperIntoHtml($id, $verticles, $thumbnail, $campaign_id, $description, $tag_id, $title) {
        $snippetFile = $this->Snippetpath . $this->getSnippetFile($verticles, $tag_id);
        $snipperId = "snipper-" . $id;
        $html = "<div id=\"$snipperId\" data-title=\"$title\" data-thumb=\"/$thumbnail\" data-cat=\"0,$campaign_id\">";
        $finalhtml = $html . $description . "</div>";
        $file = fopen($snippetFile, 'at');
        fwrite($file, $finalhtml);
        fclose($file);
        return true;
    }

    protected function editOldSnippet($id, $CSnippetFile, $SnippetFile, $thumbnail, $campaign_id, $description, $tag_id, $title) {

        ## Old File from where html will be removed
        $snippetFile = $this->Snippetpath . $this->getSnippetFile($CSnippetFile, $tag_id);
        if ($this->deletesnippethtml($id, $snippetFile) == FALSE) {
            return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_update_without_html_success'));
        }

        ## New File where html will be added
        $this->addSnipperIntoHtml($id, $SnippetFile, $thumbnail, $campaign_id, $description, $tag_id, $title);
    }

    protected function editOldSnippetwithTag_id($id, $Cverticles, $verticles, $thumbnail, $campaign_id, $description, $tag_id, $Ctag_id, $title) {

        $CsnippetFile = $this->Snippetpath . $this->getSnippetFile($Cverticles, $Ctag_id);
        if ($this->deletesnippethtml($id, $CsnippetFile) == FALSE) {
            return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_update_without_html_success'));
        }

        $snippetFile = $this->Snippetpath . $this->getSnippetFile($verticles, $tag_id);
        $this->UpdateSnipperIntoHtml($id, $snippetFile, $thumbnail, $campaign_id, $description, $title);
        return true;
    }

    protected function editOldSnippetwithCTag_id($id, $Cverticles, $verticles, $thumbnail, $campaign_id, $description, $tag_id, $Ctag_id, $title) {

        ## Old File from where html will be removed
        $CsnippetFile = $this->Snippetpath . $this->getSnippetFile($Cverticles, $Ctag_id);
        if ($this->deletesnippethtml($id, $CsnippetFile) == FALSE) {
            return Redirect::route('snippets.list')->withMessage(Config::get('acl_messages.flash.success.snippet_update_without_html_success'));
        }
        ## New File where html will be added
        $snippetFile = $this->Snippetpath . $this->getSnippetFile($verticles, $tag_id);
        $this->UpdateSnipperIntoHtml($id, $snippetFile, $thumbnail, $campaign_id, $description, $title);
    }

    protected function deletesnippethtml($id, $resource) {
        $SnippetId = "snipper-" . $id;
        $simObj = new simple_html_dom();
        $simObj->load_file($resource);
        $SnippetPortion = $simObj->find("div[id=$SnippetId]", 0);
        if (isset($SnippetPortion) && !empty($SnippetPortion)) {
            $SnippetPortion->outertext = "";
            $simObj->save($resource);
            return true;
        }
        return false;
    }

    protected function UpdateSnipperIntoHtml($id, $snippetFile, $thumbnail, $campaign_id, $description, $title) {
        // $snippetFile = $this->Snippetpath . $this->getSnippetFile($verticles, $tag_id);
        $snipperId = "snipper-" . $id;
        $html = "<div id=\"$snipperId\" data-title=\"$title\" data-thumb=\"/$thumbnail\" data-cat=\"0,$campaign_id\">";
        $finalhtml = $html . $description . "</div>";
        $file = fopen($snippetFile, 'at');
        fwrite($file, $finalhtml);
        fclose($file);
        return true;
    }

    protected function removetags($description) {

        return str_replace(array("<br>", "<br />", "<br/>"), null, ($description));
    }

}
