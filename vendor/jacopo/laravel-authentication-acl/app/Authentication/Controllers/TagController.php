<?php

namespace LaravelAcl\Authentication\Controllers;

/**
 * Class TagController
 *
 * @author Sahbaj Uddin
 */
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use LaravelAcl\Authentication\Presenters\TagPresenter;
use LaravelAcl\Library\Form\FormModel;
use LaravelAcl\Authentication\Helpers\FormHelper;
use LaravelAcl\Authentication\Models\Tag;
use LaravelAcl\Authentication\Exceptions\UserNotFoundException;
use LaravelAcl\Library\Validators\ValidatorInterface;
use LaravelAcl\Authentication\Validators\TagValidator;
use LaravelAcl\Authentication\Repository\SentryTagRepository;
use LaravelAcl\Library\Exceptions\JacopoExceptionsInterface;
use View,
    Redirect,
    App,
    Config;
//use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;


class TagController extends Controller {

    /**
     * @var \LaravelAcl\Authentication\Repository\SentryTagRepository
     */
    protected $tags;
    protected $tag_validator;

    /**
     * @var \LaravelAcl\Authentication\Helpers\FormHelper
     */
    protected $form_helper;

    /**
     * @var use LaravelAcl\Authentication\Interfaces\AuthenticateInterface;
     */
    protected $auth;

    public function __construct(TagValidator $v, FormHelper $fh, SentryTagRepository $tags) {
        $this->tags = $tags;
        $this->tag_validator = $v;
        $this->f = App::make('form_model', [$this->tag_validator, $this->tags]);
        $this->form_helper = $fh;
//$this->auth = $auth;
        
    }

    public function getList(Request $request) {
        $tags = $this->tags->all($request->except(['page']));
        return View::make('laravel-authentication-acl::admin.tags.index')->with(["tags" => $tags, "request" => $request]);
    }

    

    /**
     * Destroy the given tag.
     *
     * @param  Request  $request
     * @param  Tag  $tag
     * @return Response
     */
    public function deleteTag(Request $request) {        
        $this->tags->delete($request->get('id'));        
        return Redirect::route('tags.list')->withMessage(Config::get('acl_messages.flash.success.tag_delete_success'));
    }

    public function editTag(Request $request) {
        try {
            $obj = $this->tags->find($request->get('id'));
        } catch (UserNotFoundException $e) {
            $obj = new Tag;
        }
        $presenter = new TagPresenter($obj);

        return View::make('laravel-authentication-acl::admin.tags.edit')->with(["tag" => $obj, "presenter" => $presenter]);
    }

    public function newTag(Request $request) {
        return View::make('laravel-authentication-acl::admin.tags.new');
    }

    public function postEditTag(Request $request) {
        $id = $request->get('id');

        try {
            $obj = $this->f->process($request->all());
        } catch (JacopoExceptionsInterface $e) {
            $errors = $this->f->getErrors();
            // passing the id incase fails editing an already existing item
            return Redirect::route("tags.edit", $id ? ["id" => $id] : [])->withInput()->withErrors($errors);
        }
        return Redirect::route('tags.edit', ["id" => $obj->id])->withMessage(Config::get('acl_messages.flash.success.tag_edit_success'));
    }

    public function postTag(Request $request) {
        try {
            $obj = $this->f->process($request->all());
        } catch (JacopoExceptionsInterface $e) {
            $errors = $this->f->getErrors();            
            // passing the id incase fails editing an already existing item
            return Redirect::route("tags.new", [])->withInput()->withErrors($errors);
        }
        return Redirect::route('tags.list', ["id" => $obj->id])->withMessage(Config::get('acl_messages.flash.success.tag_edit_success'));
    }

}
