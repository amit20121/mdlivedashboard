<?php

namespace LaravelAcl\Authentication\Controllers;

use Illuminate\Http\Request;
use LaravelAcl\Authentication\Repository\ContentRepository;
use View;
use Backpack\PageManager\app\Models\Page;
class DashboardController extends Controller {

    protected $contents;

    public function __construct(ContentRepository $contents) {

        $this->contents = $contents;
    }

    public function base(Request $request) {        
        $authentication = \App::make('authentication_helper');
        $auth = \App::make('authenticator');
        $user = $auth->getLoggedUser();
        $verticles = str_replace(" ", "_",strtolower($user->groups()->first()->name));               
        if ($authentication->hasPermission(array('_template-editor'))) {
            $page = Page::findBySlugOrId('marcom-builder');    
            $contents = $this->contents->all($request->except(['page']), $verticles);
            return View::make('laravel-authentication-acl::admin.dashboard.template-campaign')->with(["contents" => $contents, "request" => $request,'page' => $page->withFakes()]);
        } else
            return View::make('laravel-authentication-acl::admin.dashboard.default')->with([]);
    }

    public function index(Request $request) {        
        $authentication = \App::make('authentication_helper');
        if ($authentication->hasPermission(array('_template-editor'))) {
            $campaigns = $this->campaigns->all($request->except(['page']));
            return View::make('admin.dashboard.template-campaign')->with(["campaigns" => $campaigns, "request" => $request]);
        } else
        return View::make('admin.dashboard.default');
    }

    public function pdf(Request $request) {
//        $authentication = \App::make('authentication_helper');
//
//        //$pdf3 = \App::make('dompdf.wrapper');
//        //$pdf3->loadFile('/var/www/html/mdlive_content/MDLIVE-Monthly-Flyer-Cold-and-Flu-Employer.html');
//        //return $pdf3->download();
        $pdf2 = \App::make('snappy.image.wrapper');
        $pdf2->loadFile('http://mdlivemarketinghub.ml:88/admin/templates/list');
        return $pdf2->download();
//        $pdf = \App::make('snappy.pdf.wrapper');
//        $pdf->loadFile('http://localhost/mdlive_content/MDLIVE-Monthly-Flyer-Cold-and-Flu-Employer.html');
//        return $pdf->download();
    }

}
