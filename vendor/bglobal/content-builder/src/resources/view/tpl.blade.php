<link href="{{ config('content-builder.style_css') }}" rel="stylesheet" type="text/css" />
<link href="{{ config('content-builder.content_css') }}" rel="stylesheet" type="text/css" />
<link href="{{ config('content-builder.contentbuilder_css') }}" rel="stylesheet" type="text/css" />
<link href="{{ config('content-builder.font_css') }}" rel="stylesheet" type="text/css" />
<link href="{{ config('content-builder.bootstrapcdn_css') }}" rel="stylesheet" type="text/css" />
<link href="{{ config('content-builder.googlefont_css') }}" rel="stylesheet" type="text/css" />

{{-- <script type="text/javascript" src="{{ config('content-builder.jquery') }}"></script> --}}
<script type="text/javascript" src="{{ config('content-builder.jquery-ui') }}"></script>
<script type="text/javascript" src="{{ config('content-builder.contentbuilder-src') }}"></script>
<script type="text/javascript" src="{{ config('content-builder.saveimages') }}"></script>
<script type="text/javascript">

@if (isset($els) && $els !="")
        
        jQuery(document).ready(function ($) {
            $("#contentarea").contentbuilder({
                @foreach(config('content-builder.'.$els) as $key => $val)
                    {!! $key !!}: '{!! $val !!}',
                @endforeach
        });
        });
        
@else
    jQuery(document).ready(function ($) {
    $("#contentarea").contentbuilder({
    @foreach(config('content-builder.default') as $key => $val)
    {!! $key !!}: '{!! $val !!}',
            @endforeach
            snippetCategories: [
                    [0, "Default"],
                    [1, "Cold and Flu"],
                    [2, "Sinus"],
                    [3, "Mosquito-Borne Illness"],
                    [4, "Stress"],
                    [5, "Allergy"],
                    [6, "UTI"],
                    [7, "Assessment"],
                    [8, "Pink Eyes"],
                    [9, "Relationship Councelling"],
                    [10, "Summar"],
                    [11, "Back to School"],
            ],
    });
});
@endif

</script>
